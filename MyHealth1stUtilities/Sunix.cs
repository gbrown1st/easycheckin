﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace MyHealth1stUtilities
{
    class Sunix
    {
        string connectionString;

        const string GET_YESTERDAYS_APPTS = @"SELECT
  Trim(vAppoint.LOCATION) AS BrachIdentifier,
  Trim(vAppoint.REFNUM) AS PMSPatientID,
  Trim(vDoctor.CODE) AS PMSUserIdentifier,
  vAppoint.BOOKDATE AS RecitpientLastVisitDate,
  Trim(vAppoint.STATUS) AS RecipientWaitSeen,
  (Trim(vDoctor.TITLE) + ' ' + Trim(vDoctor.GNAME) + ' ' + Trim(vDoctor.SNAME)) AS PractitionerName,
  '' AS StoreName,
  '' AS StoreAddress,
  '' AS StoreSuburb,
  '' AS StoreState,
  '' AS StorePostcode,
  vAppoint.GIVEN_NAME AS RecitpientFirstName,
  vAppoint.SNAME AS RecitpientLastName,
  vPatient.STATE AS RecipientState,
  vPatient.POSTCODE AS RecipientPostcode,
  vPatient.EMAIL AS RecipientEmail,
  vPatient.BIRTHDAY AS RecipientDOB,
  vPatient.SEX AS RecipientGender,
  vPatient.HFUND AS RecipientHealthFund
FROM
  vAppoint
  INNER JOIN vDoctor ON vAppoint.OPTOM = vDoctor.CODE
  INNER JOIN vPatient ON vAppoint.REFNUM = vPatient.REFNUM
  INNER JOIN VAPEMAIL ON vAppoint.PKEY = VAPEMAIL.PKEY
WHERE
  vAppoint.BOOKDATE = {0} AND
  vAppoint.STATUS = 'Book'";

        public Sunix (string connectionString)
        {
            this.connectionString = connectionString;
        }

        private OleDbConnection GetConnection()
        {
            var connection = new OleDbConnection(connectionString);
            return connection;
        }

        public List<EmailRecipient> getYesterdaysAppts()
        {
            var connection = new OleDbConnection(connectionString);
            LogWriter.Logger.Info("getYesterdaysAppts = " + connectionString);
            var yesterday = DateTime.Now.AddDays(-1).ToString("{^yyyy/MM/dd}");
            var sql = string.Format(GET_YESTERDAYS_APPTS, yesterday);
            LogWriter.Logger.Info("GET_YESTERDAYS_APPTS = " + sql);
            var emailRecipientList = new List<EmailRecipient>();
            try
            {
                using (connection)
                {
                    connection.Open();
                    var command = new OleDbCommand(sql, connection);
                    OleDbDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var er = new EmailRecipient();
                        er.BranchIdentifier = reader.GetString(0);
                        er.BranchIdentifier = er.BranchIdentifier.Trim();
                        er.PMSPatientID = reader.GetString(1);
                        er.PMSPatientID = er.PMSPatientID.Trim();
                        er.PMSUserIdentifier = reader.GetString(2);
                        er.PMSUserIdentifier = er.PMSUserIdentifier.Trim();
                        er.RecipientLastVisitDate = reader.GetDateTime(3).ToString("yyyy-MM-dd");
                        er.RecipientWaitseen = reader.GetString(4);
                        er.PractitionerName = reader.GetString(5);
                        er.PractitionerName = er.PractitionerName.Trim();
                        er.StoreName = reader.GetString(6);
                        er.StoreAddress = reader.GetString(7);
                        er.StoreSuburb = reader.GetString(8);
                        er.StoreState = reader.GetString(9);
                        er.StorePostcode = reader.GetString(10);
                        er.RecipientFirstName = reader.GetString(11);
                        er.RecipientFirstName = er.RecipientFirstName.Trim();
                        er.RecipientLastName = reader.GetString(12);
                        er.RecipientLastName = er.RecipientLastName.Trim();
                        er.RecipientPostcode = reader.GetString(13);
                        er.RecipientPostcode = er.RecipientPostcode.Trim();
                        er.RecipientState = reader.GetString(14);
                        er.RecipientState = er.RecipientState.Trim();
                        er.RecipientEmail = reader.GetString(15);
                        er.RecipientEmail = er.RecipientEmail.Trim();
                        er.RecipientDOB = reader.GetDateTime(16).ToString("yyyy-MM-dd");
                        er.RecipientGender = reader.GetString(17);
                        er.RecipientHealthFund = reader.GetString(18);
                        er.RecipientHealthFund = er.RecipientHealthFund.Trim();
                        emailRecipientList.Add(er);
                    }

                    // Always call Close when done reading.
                    reader.Close();
                }

                connection.Close();

            }

            catch (Exception e)
            {
                LogWriter.Logger.Info(e.ToString());
            }

            return emailRecipientList;
        }

    }
}
