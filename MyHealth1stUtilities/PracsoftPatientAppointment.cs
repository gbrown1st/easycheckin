﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHealth1stUtilities
{
    public class PracsoftPatientAppointment
    {
        public int ApptID { get; set; }
        public string PractitionerID { get; set; }
        public int PatientID { get; set; }
        public string When { get; set; }
        public string FIRST_NAME { get; set; }
        public string SURNAME { get; set; }
        public string GENDER { get; set; }
        public string DOB { get; set; }
        public string MEDICARE_NO { get; set; }
        public string PHONE_MOBILE { get; set; }
        public string STREET_LINE_1 { get; set; }
        public string CITY { get; set; }
        public string POSTCODE { get; set; }
        public string Practitioner { get; set; }
        public string name { get; set; }
        public int numberWaiting { get; set; }

        public PracsoftPatientAppointment()
        {
        }

        override public string ToString()
        {
            return string.Format("ApptID: {0} - PractitionerID: {1} - PatientID: {2} - When: {3} - FIRST_NAME: {4} - SURNAME: {5} - GENDER: {6} - DOB: {7} - MEDICARE_NO: {8} - PHONE_MOBILE: {9} - STREET_LINE_1: {10} - CITY: {11} - POSTCODE: {12} - Practitioner: {13} - name: {14} - numberWaiting: {15}", ApptID, PractitionerID, PatientID, When, FIRST_NAME, SURNAME, GENDER, DOB, MEDICARE_NO, PHONE_MOBILE, STREET_LINE_1, CITY, POSTCODE, Practitioner, name, numberWaiting);
        }

    }
}