﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHealth1stUtilities
{
    /// <summary>
	/// Description of PracsoftAppointment.
	/// </summary>
	public class PracsoftAppointment
    {
        public int practitionerID { get; set; }
        public int apptID { get; set; }
        public int patientID { get; set; }
        public string descrip { get; set; }
        public string when { get; set; }

        public PracsoftAppointment()
        {
        }

        override public string ToString()
        {
            return string.Format("practitionerID: {0} - apptID: {1} - patientID: {2} - descrip: {3} - when: {4}", practitionerID, apptID, patientID, descrip, when);
        }
    }
}
