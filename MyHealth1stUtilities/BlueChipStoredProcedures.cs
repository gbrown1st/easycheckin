﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace MyHealth1stUtilities
{
    class BlueChipStoredProcedures
    {
        String connectionString;
        
        const string GET_APPOINTMENT_BY_GENDER_DOB = @"SELECT 
        BCPeAppt.ApptID, BCPeAppt.PractID, BCPeAppt.PatientID, '' AS 'When', 
        BCPePatient.Given AS FIRST_NAME, BCPePatient.Surname AS SURNAME, 
        CASE BCPePatient.Gender 
                      WHEN 'M' THEN 'Male' 
                      WHEN 'F' THEN 'Female'  
                      ELSE 'Not stated' 
        END AS GENDER ,
        CONVERT(DATE, DATEADD(DAY, BCPePatient.DOB - 693596, '01/01/1900')) AS DOB,
        BCPePatient.MedicareCardNo AS MEDICARE_NO, BCPePatient.MobilePhone AS PHONE_MOBILE,
        BCPePatient.Address1 AS STREET_LINE_1, BCPeSuburb.Name AS CITY, BCPeSuburb.Postcode AS POSTCODE,
        (BCPePract.Title+' '+BCPePract.Given+' '+BCPePract.Surname) AS Practitioner,
        (BCPePract.Title+' '+BCPePract.Given+' '+BCPePract.Surname) AS Name,
        (select count(BCPeAppt.ApptID) 
        from BCPeAppt
        WHERE        
        (BCPeAppt.Date = DATEDIFF(DAY, '1/1/1900', CONVERT(VARCHAR, GETDATE())) + 693596) 
        and BCPeAppt.PractID = BCPePract.PractID
        and BCPeAppt.Status = 1)  as NUMBERWAITING
        FROM
        BCPeAppt INNER JOIN
        BCPePatient ON BCPeAppt.PatientID = BCPePatient.PatientID INNER JOIN
        BCPePract ON BCPeAppt.PractID = BCPePract.PractID INNER JOIN
        BCPeSuburb ON BCPePatient.SuburbID = BCPeSuburb.SuburbID
        WHERE        
        (BCPeAppt.Date = DATEDIFF(DAY, '1/1/1900', CONVERT(VARCHAR, GETDATE())) + 693596) 
        AND (BCPePatient.DOB = DATEDIFF(DAY, '1/1/1900', @dob) + 693596) 
        AND (BCPePatient.Gender = @gender)";

        const string GET_APPOINTMENT_BY_MEDICARENO = @"SELECT 
        BCPeAppt.ApptID, BCPeAppt.PractID, BCPeAppt.PatientID, '' AS 'When', 
        BCPePatient.Given AS FIRST_NAME, BCPePatient.Surname AS SURNAME, 
        CASE BCPePatient.Gender 
                      WHEN 'M' THEN 'Male' 
                      WHEN 'F' THEN 'Female'  
                      ELSE 'Not stated' 
        END AS GENDER ,
        CONVERT(DATE, DATEADD(DAY, BCPePatient.DOB - 693596, '01/01/1900')) AS DOB,
        BCPePatient.MedicareCardNo AS MEDICARE_NO, BCPePatient.MobilePhone AS PHONE_MOBILE,
        BCPePatient.Address1 AS STREET_LINE_1, BCPeSuburb.Name AS CITY, BCPeSuburb.Postcode AS POSTCODE,
        (BCPePract.Title+' '+BCPePract.Given+' '+BCPePract.Surname) AS Practitioner,
        (BCPePract.Title+' '+BCPePract.Given+' '+BCPePract.Surname) AS Name,
        (select count(BCPeAppt.ApptID) 
        from BCPeAppt
        WHERE        
        (BCPeAppt.Date = DATEDIFF(DAY, '1/1/1900', CONVERT(VARCHAR, GETDATE())) + 693596) 
        and BCPeAppt.PractID = BCPePract.PractID
        and BCPeAppt.Status = 1)  as NUMBERWAITING
        FROM
        BCPeAppt INNER JOIN
        BCPePatient ON BCPeAppt.PatientID = BCPePatient.PatientID INNER JOIN
        BCPePract ON BCPeAppt.PractID = BCPePract.PractID INNER JOIN
        BCPeSuburb ON BCPePatient.SuburbID = BCPeSuburb.SuburbID
        WHERE        
        (BCPeAppt.Date = DATEDIFF(DAY, '1/1/1900', CONVERT(VARCHAR, GETDATE())) + 693596) 
        AND (REPLACE(BCPePatient.MedicareCardNo, ' ', '') = @medicareNo)";

        const string ADD_TO_WAITROOM = @"UPDATE BCPeAppt
        set BCPeAppt.Status = 1
        where ApptID = @ApptID";

        public BlueChipStoredProcedures (String connectionString)
        {
            this.connectionString = connectionString;
        }

        private SqlConnection GetConnection()
        {
            var connection = new SqlConnection(this.connectionString);
            return connection;
        }

        
        public string getAppointmentIDByGenderDOB(string gender, string dob)
        {
            SqlConnection connection = GetConnection();
            var json = "";

            using (connection)
            {
                var command = new SqlCommand(GET_APPOINTMENT_BY_GENDER_DOB, connection);
                command.Parameters.AddWithValue("@dob", dob);
                command.Parameters.AddWithValue("@gender", gender);
                var result = new List<PracsoftPatientAppointment>();
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

        public string getAppointmentIDByMedicaredNumber(string medicareNumber)
        {
            SqlConnection connection = GetConnection();
            var json = "";

            using (connection)
            {
                var command = new SqlCommand(GET_APPOINTMENT_BY_MEDICARENO, connection);
                command.Parameters.AddWithValue("@medicareNo", medicareNumber);
                var result = new List<PracsoftPatientAppointment>();
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

        public string HideCharacters(string str)
        {
            LogWriter.Logger.Info("HideCharacters = " + str);
            var strArray = str.Split(' ');
            var strToHide = strArray[1];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= strToHide.Length - 1; i++)
            {
                sb.AppendFormat("{0}", "*");
            }
            var returnStr = strArray[0] + " " + sb.ToString() + " " + strArray[2];
            return returnStr;
        }
        
        public int addToWaitroom(int apptID)
        {

            SqlConnection connection = GetConnection();
            int ret = 0;
            using (connection)
            {
                var command = new SqlCommand(ADD_TO_WAITROOM, connection);
                connection.Open();
                command.Parameters.AddWithValue("@apptID", apptID);

                ret = command.ExecuteNonQuery();
                return ret;
            }

        }
        
    }
}