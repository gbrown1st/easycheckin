﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyHealth1stUtilities
{
    public class BestPracticeSMSReminder
    {
        public int PATIENTID { get; set; }
        public string FIRSTNAME { get; set; }
        public string SURNAME { get; set; }
        public string MOBILEPHONE { get; set; }
        public string APPOINTMENTDATE { get; set; }
        public int APPOINTMENTTIME { get; set; }
        public string PRACTITIONER { get; set; }
        public string PRACTICENAME { get; set; }

        public BestPracticeSMSReminder()
        {
        }

        override public string ToString()
        {
            return string.Format("PATIENTID: {0} - FIRSTNAME: {1} - SURNAME: {2} - MOBILEPHONE: {3} - APPOINTMENTDATE: {4} - APPOINTMENTTIME: {5} - PRACTITIONER: {6} - PRACTICENAME: {7}", PATIENTID, FIRSTNAME, SURNAME, MOBILEPHONE, APPOINTMENTDATE, APPOINTMENTTIME, PRACTITIONER, PRACTICENAME);
        }
    }
}
