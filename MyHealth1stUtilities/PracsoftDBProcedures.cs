﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace MyHealth1stUtilities
{
    /// <summary>
	/// Description of DBProcedures
	/// </summary>
	public class PracsoftDBProcedures
    {
        String connectionString;

        const string GET_PATIENT_BY_MEDICARE_NO = @"SELECT
        PATIENT_ID FROM CM_PATIENT
        WHERE (MEDICARE_NO = @medicareNo)";

        const string GET_PATIENT_ID = @"SELECT PATIENT_ID
            FROM CM_PATIENT
            WHERE (UPPER(FIRST_NAME) = @firstName)
            AND (UPPER(SURNAME) = @lastName)
            AND CONVERT(CHAR(10),[DOB],126) = @dob
            AND GENDER_CODE = @gender";

        const string GET_PATIENT_ID_BY_GENDER_DOB = @"SELECT PATIENT_ID
            FROM CM_PATIENT
            WHERE CONVERT(CHAR(10),[DOB],126) = @dob
            AND GENDER_CODE = @gender";

        const string GET_APPOINTMENT_BY_GENDER_DOB = @"SELECT 
            APPT.ApptID, APPT.PractitionerID, APPT.PatientID, APPT.[When], 
            CM_PATIENT.FIRST_NAME, CM_PATIENT.SURNAME, 
            CASE GENDER_CODE 
                      WHEN 'M' THEN 'Male' 
                      WHEN 'F' THEN 'Female'  
                      ELSE 'Not stated' 
            END as GENDER ,
            CONVERT(CHAR(10),[DOB],126)as DOB, 
            CM_PATIENT.MEDICARE_NO, CM_PATIENT.PHONE_MOBILE, 
            CM_PATIENT.STREET_LINE_1, CM_PATIENT.CITY, CM_PATIENT.POSTCODE, 
            PRAC.Practitioner, PRAC.Name,
            (SELECT COUNT(WAITROOM.ID)
            FROM WAITROOM 
            where WAITROOM.DrCode = PRAC.Practitioner
            and YEAR(WAITROOM.ApptTime) = YEAR(Getdate())
            and MONTH(WAITROOM.ApptTime) = MONTH(Getdate())
            and DAY(WAITROOM.ApptTime) = DAY(Getdate())) as NUMBERWAITING
            FROM APPT INNER JOIN
            CM_PATIENT ON APPT.PatientID = CM_PATIENT.PATIENT_ID INNER JOIN
            PRAC ON APPT.PractitionerID = PRAC.PractitionerID
            WHERE CONVERT(date,[When])=CONVERT(date,getdate())
            AND CONVERT(CHAR(10),[DOB],126) = @dob
            AND GENDER_CODE = @gender
            ORDER BY APPT.[When]";

        const string GET_APPOINTMENT_BY_MEDICARENO = @"SELECT 
            APPT.ApptID, APPT.PractitionerID, APPT.PatientID, APPT.[When], 
            CM_PATIENT.FIRST_NAME, CM_PATIENT.SURNAME, 
            CASE GENDER_CODE 
                      WHEN 'M' THEN 'Male' 
                      WHEN 'F' THEN 'Female'  
                      ELSE 'Not stated' 
            END as GENDER ,
            CONVERT(CHAR(10),[DOB],126)as DOB, 
            CM_PATIENT.MEDICARE_NO, CM_PATIENT.PHONE_MOBILE, 
            CM_PATIENT.STREET_LINE_1, CM_PATIENT.CITY, CM_PATIENT.POSTCODE, 
            PRAC.Practitioner, PRAC.Name,
            (SELECT COUNT(WAITROOM.ID)
            FROM WAITROOM 
            where WAITROOM.DrCode = PRAC.Practitioner
            and YEAR(WAITROOM.ApptTime) = YEAR(Getdate())
            and MONTH(WAITROOM.ApptTime) = MONTH(Getdate())
            and DAY(WAITROOM.ApptTime) = DAY(Getdate())) as NUMBERWAITING
            FROM APPT INNER JOIN
            CM_PATIENT ON APPT.PatientID = CM_PATIENT.PATIENT_ID INNER JOIN
            PRAC ON APPT.PractitionerID = PRAC.PractitionerID
            WHERE CONVERT(date,[When])=CONVERT(date,getdate())
            AND CM_PATIENT.MEDICARE_NO = @medicareNo
            ORDER BY APPT.[When]";

        const string GET_IDENT_CURRENT = "Select IDENT_CURRENT('WaitRoom') + 1 as DISPLAY_ORDER";

        const string GET_PRACTITIONER_CODE = @"SELECT PRAC.Practitioner 
            FROM PRAC 
            WHERE PRAC.PractitionerID = @practitionerID";

        const string ADD_TO_WAITROOM = @"insert into WAITROOM 
            (PatientNo, DISPLAY_ORDER, [Type], ApptIDList, DrCode,DaysOverDue, ArriveTime, ApptTime) 
            values (@patientID, @displayOrder, 0, @apptID, @practitionerCode, -9999, getDate(), @apptTime)";

        const string UPDATE_APPOINTMENT = "update APPT set Flag = 2, TimeInWAITROOM = getDate() where ApptID = @apptID";

        const string GET_IN_WAITROOM = @"SELECT ID 
            FROM WAITROOM 
            WHERE (ApptIDList = @apptListID)";

        const string COUNT_YESTERDAYS_APPTS = @"select COUNT(*) as number_of_appts, 
        '1stA' as appt_type from APPT 
        where [Descrip] like '%1stA%' 
        and DATEDIFF(DAY, DATEADD(DAY, -1, CURRENT_TIMESTAMP), [When]) = 0 
        UNION 
        select COUNT(*) as number_of_appts,
        'PMS' as appt_type from APPT
        where [Descrip] not like '%1stA%'
        and DATEDIFF(DAY, DATEADD(DAY, -1, CURRENT_TIMESTAMP), [When]) = 0";

        const string GET_WAITING_ROOM = @"SELECT DATEDIFF(hh, [TimeInWAITROOM], GETDATE()) as waitTimeHours,
DATEDIFF(mi, [TimeInWAITROOM], GETDATE())%60 as waitTimeMins
FROM WAITROOM INNER JOIN APPT ON WAITROOM.ApptIDList = APPT.ApptID
WHERE [TimeInWAITROOM] is not null AND Flag = 2
ORDER BY WAITROOM.DISPLAY_ORDER";

        const string GET_AVG_WAITING_TIME = @"SELECT
	AVG(
		DATEDIFF(
			MINUTE ,
			WAITROOM.ArriveTime ,
			CM_PATIENT_LOCK.STAMP_CREATED_DATETIME
		)
	) AS waittime
FROM
	CM_PATIENT_LOCK
INNER JOIN WAITROOM ON CM_PATIENT_LOCK.PATIENT_ID = WAITROOM.PatientNo
WHERE
	(WAITROOM.ItemNos = '')
OR(WAITROOM.ItemNos IS NULL)";


        public static string GET_APPOINTMENT_BY_GENDER_DOB1 => GET_APPOINTMENT_BY_GENDER_DOB;

        public static string GET_APPOINTMENT_BY_GENDER_DOB2 => GET_APPOINTMENT_BY_GENDER_DOB;

        public PracsoftDBProcedures(String connectionString)
        {
            this.connectionString = connectionString;
        }

        private SqlConnection GetConnection()
        {
            var connection = new SqlConnection(this.connectionString);
            return connection;
        }

        public int getPatientID(string firstName, string lastName, string dob, string gender)
        {

            SqlConnection connection = GetConnection();
            int patientID = 0;
            firstName = firstName.ToUpper();
            firstName = firstName.Trim();
            lastName = lastName.ToUpper();
            lastName = lastName.Trim();
            gender = gender.Substring(0, 1);
            gender = gender.ToUpper();
            using (connection)
            {
                var command = new SqlCommand(GET_PATIENT_ID, connection);
                command.Parameters.AddWithValue("@firstName", firstName);
                command.Parameters.AddWithValue("@lastName", lastName);
                command.Parameters.AddWithValue("@dob", dob);
                command.Parameters.AddWithValue("@gender", gender);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    patientID = (int)reader["PATIENT_ID"];
                    return patientID;
                }
                reader.Close();
            }
            return patientID;
        }

        public string getAppointmentIDByGenderDOB(string gender, string dob)
        {
            SqlConnection connection = GetConnection();
            var json = "";

            using (connection)
            {
                var command = new SqlCommand(GET_APPOINTMENT_BY_GENDER_DOB1, connection);
                command.Parameters.AddWithValue("@dob", dob);
                command.Parameters.AddWithValue("@gender", gender);
                var result = new List<PracsoftPatientAppointment>();
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

        public string getAppointmentIDByMedicaredNumber(string medicareNumber)
        {
            SqlConnection connection = GetConnection();
            var json = "";

            using (connection)
            {
                var command = new SqlCommand(GET_APPOINTMENT_BY_MEDICARENO, connection);
                command.Parameters.AddWithValue("@medicareNo", medicareNumber);
                var result = new List<PracsoftPatientAppointment>();
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

        public string HideCharacters(string str)
        {
            LogWriter.Logger.Info("HideCharacters = " + str);
            var strArray = str.Split(' ');
            var strToHide = strArray[1];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= strToHide.Length - 1; i++)
            {
                sb.AppendFormat("{0}", "*");
            }
            var returnStr = strArray[0] + " " + sb.ToString() + " " + strArray[2];
            return returnStr;
        }

        public int getIndentCurrent()
        {

            SqlConnection connection = GetConnection();
            int displayOrder = 0;

            using (connection)
            {
                var command = new SqlCommand(GET_IDENT_CURRENT, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    displayOrder = Convert.ToInt32(reader["DISPLAY_ORDER"]);
                }
                reader.Close();
                return displayOrder;
            }
        }

        public string getPractitonerCode(int practitionerID)
        {

            SqlConnection connection = GetConnection();
            string practitionerCode = "";

            using (connection)
            {
                var command = new SqlCommand(GET_PRACTITIONER_CODE, connection);
                command.Parameters.AddWithValue("@practitionerID", practitionerID);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    practitionerCode = (string)reader["Practitioner"];
                    return practitionerCode;
                }
                reader.Close();
            }
            return practitionerCode;
        }

        public int getWaitroomID(string apptListID)
        {

            SqlConnection connection = GetConnection();
            int ID = 0;

            using (connection)
            {
                var command = new SqlCommand(GET_IN_WAITROOM, connection);
                command.Parameters.AddWithValue("@apptListID", apptListID);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ID = Convert.ToInt32(reader["ID"]);
                }
                reader.Close();
                return ID;
            }

        }

        public int addToWaitroom(int patientID, int displayOrder, int apptID, string practitionerCode, string apptTime)
        {

            SqlConnection connection = GetConnection();
            int ret = 0;
            using (connection)
            {
                var command = new SqlCommand(ADD_TO_WAITROOM, connection);
                connection.Open();
                command.Parameters.AddWithValue("@patientID", patientID);
                command.Parameters.AddWithValue("@displayOrder", displayOrder);
                command.Parameters.AddWithValue("@apptID", apptID);
                command.Parameters.AddWithValue("@practitionerCode", practitionerCode);
                command.Parameters.AddWithValue("@apptTime", apptTime);

                ret = command.ExecuteNonQuery();
                return ret;
            }

        }

        public int updateAppointment(int apptID)
        {
            SqlConnection connection = GetConnection();
            int ret = 0;

            using (connection)
            {
                var command = new SqlCommand(UPDATE_APPOINTMENT, connection);
                command.Parameters.AddWithValue("@apptID", apptID);
                connection.Open();
                ret = command.ExecuteNonQuery();
                return ret;
            }
        }

        public string countYesterdaysAppts()
        {
            var connection = new SqlConnection(this.connectionString);
            string json = "";
            using (connection)
            {
                var command = new SqlCommand(COUNT_YESTERDAYS_APPTS, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

        public string getWaiting()
        {
            var connection = new SqlConnection(this.connectionString);
            string json = "";
            using (connection)
            {
                var command = new SqlCommand(GET_WAITING_ROOM, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

        public string getAverageWaiting()
        {
            var connection = new SqlConnection(this.connectionString);
            string json = "";
            using (connection)
            {
                var command = new SqlCommand(GET_AVG_WAITING_TIME, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

    }
}
