﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHealth1stUtilities
{
    class Optomate
    {
        string connectionString;

        const string GET_YESTERDAYS_APPTS = @"SELECT   
					  BRANCH.IDENTIFIER AS BranchIdentifier,
		              PATIENT.ID AS PMSPatientID, 
        			  USERS.IDENTIFIER as PMSUserIdentifier, 
					  APPOINTMENT.STARTDATE AS RecipientLastVisitDate,
					  APPOINTMENT.APP_PROGRESS AS RecipientWaitseen,
					  USERS.FULL_NAME AS PractitionerName,  
                      BRANCH.NAME AS StoreName, 
					  BRANCH.ADDRESS AS StoreAddress, 
					  BRANCH.SUBURB AS StoreSuburb,  
                      BRANCH.STATE AS StoreState, 
					  BRANCH.POSTCODE AS StorePostcode,  
                      PATIENT.GIVEN AS RecipientFirstName, 
					  PATIENT.SURNAME AS RecipientLastName, 
					  PATIENT.RESIDENT_POSTCODE AS RecipientPostcode,  
                      PATIENT.RESIDENT_STATE AS RecipientState, 
                      PATIENT.EMAIL AS RecipientEmail,  
					  PATIENT.BIRTHDATE AS RecipientDOB, 
                      PATIENT.GENDER AS RecipientGender,  
                      PATIENT.HEALTHFUND_IDENTIFIER AS RecipientHealthFund
                      FROM         APPOINTMENT WITH (NOLOCK) INNER JOIN 
                      PATIENT WITH (NOLOCK) ON APPOINTMENT.PATIENTID = PATIENT.ID INNER JOIN 
                      BRANCH WITH (NOLOCK) ON APPOINTMENT.BRANCH_IDENTIFIER = BRANCH.IDENTIFIER INNER JOIN 
                      USERS WITH (NOLOCK) ON APPOINTMENT.USER_IDENTIFIER = USERS.IDENTIFIER 
                      WHERE DATEDIFF(DAY, DATEADD(DAY, -1, GETDATE()), APPOINTMENT.STARTDATE) = 0
					  and APPOINTMENT.APP_PROGRESS <> 6";

        public Optomate(string connectionString)
        {
            this.connectionString = connectionString;
        }

        private SqlConnection GetConnection()
        {
            var connection = new SqlConnection(this.connectionString);
            return connection;
        }

        private static string MyToString(object o)
        {
            if (o == DBNull.Value || o == null)
                return "";

            return o.ToString();
        }

        public List<EmailRecipient> getYesterdaysAppts()
        {
            var connection = new SqlConnection(connectionString);
            LogWriter.Logger.Info("getYesterdaysAppts = " + connectionString);
            var emailRecipientList = new List<EmailRecipient>();
            try
            {
                using (connection)
                {
                    var command = new SqlCommand(GET_YESTERDAYS_APPTS, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var er = new EmailRecipient();
                        er.BranchIdentifier = MyToString(reader.GetValue(0));
                        er.PMSPatientID = MyToString(reader.GetValue(1));
                        er.PMSUserIdentifier = MyToString(reader.GetValue(2));
                        er.RecipientLastVisitDate = MyToString(reader.GetValue(3));
                        er.RecipientWaitseen = MyToString(reader.GetValue(4));
                        er.PractitionerName = MyToString(reader.GetValue(5));
                        er.StoreName = MyToString(reader.GetValue(6));
                        er.StoreAddress = MyToString(reader.GetValue(7));
                        er.StoreSuburb = MyToString(reader.GetValue(8));
                        er.StoreState = MyToString(reader.GetValue(9));
                        er.StorePostcode = MyToString(reader.GetValue(10));
                        er.RecipientFirstName = MyToString(reader.GetValue(11));
                        er.RecipientLastName = MyToString(reader.GetValue(12));
                        er.RecipientPostcode = MyToString(reader.GetValue(13));
                        er.RecipientState = MyToString(reader.GetValue(14));
                        er.RecipientEmail = MyToString(reader.GetValue(15));
                        er.RecipientDOB = MyToString(reader.GetValue(16));
                        er.RecipientGender = MyToString(reader.GetValue(17));
                        er.RecipientHealthFund = MyToString(reader.GetValue(18));
                        emailRecipientList.Add(er);
                }

                // Always call Close when done reading.
                reader.Close();
            }


                    connection.Close();

        }

            catch (Exception e)
            {
                LogWriter.Logger.Info(e.ToString());
            }
            
            return emailRecipientList;
        }
    }
}
