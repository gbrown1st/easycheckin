﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyHealth1stUtilities
{
    public class ArrivalResponse
    {
        public string appChannel { get; set; }
        public string messageResponse { get; set; }

        public ArrivalResponse()
        {
        }

        override public string ToString()
        {
            return string.Format("appChannel: {0} - messageResponse: {1}", appChannel, messageResponse);
        }
    }
}
