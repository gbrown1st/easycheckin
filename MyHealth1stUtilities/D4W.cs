﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;

namespace MyHealth1stUtilities
{
    class D4W
    {
        string connectionString;

        const string GET_YESTERDAYS_APPTS = @"select '' as BranchIdentifier,
p.patient_id as PMSPatientID,
 '' as PMSUserIdentifier,
 a.app_date as RecipientLastVisitDate,
 '' as RecipientWaitSeen,
 b.app_book_description as PractitionerName,
 '' as StoreName,
 '' as StoreAddress,
 '' as StoreSuburb,
 '' as StoreState,
 '' as StorePostcode,
p.firstname as RecipientFirstName,
p.surname as RecipientLastName,
 '' as RecipientState,
 '' as RecipientPostcode,
p.email as RecipientEmail,
p.dob as RecipientDOB,
p.patient_sex as RecipientGender,
 '' as RecipientHealthFund
from a_appointments a inner join app_books b on a.app_book_id = b.app_book_number
inner join patients p on  a.pat_id = p.patient_id
where a.app_date = dateadd(day,-1, convert(date,getdate()) )";
        
        public D4W(string connectionString)
        {
            this.connectionString = connectionString;
        }

        private OdbcConnection GetConnection()
        {
            var connection = new OdbcConnection(connectionString);
            return connection;
        }

        public List<EmailRecipient> getYesterdaysAppts()
        {
            var connection = new OdbcConnection(connectionString);
            LogWriter.Logger.Info("getYesterdaysAppts = " + connectionString);
            var emailRecipientList = new List<EmailRecipient>();
            try
            {
                using (connection)
                {
                    connection.Open();
                    OdbcCommand command = new OdbcCommand(GET_YESTERDAYS_APPTS, connection);
                    OdbcDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        var er = new EmailRecipient();
                        er.BranchIdentifier = MyToString(reader.GetValue(0));
                        er.PMSPatientID = MyToString(reader.GetValue(1));
                        er.PMSUserIdentifier = MyToString(reader.GetValue(2));
                        er.RecipientLastVisitDate = MyToString(reader.GetValue(3));
                        er.RecipientWaitseen = MyToString(reader.GetValue(4));
                        er.PractitionerName = MyToString(reader.GetValue(5));
                        er.StoreName = MyToString(reader.GetValue(6));
                        er.StoreAddress = MyToString(reader.GetValue(7));
                        er.StoreSuburb = MyToString(reader.GetValue(8));
                        er.StoreState = MyToString(reader.GetValue(9));
                        er.StorePostcode = MyToString(reader.GetValue(10));
                        er.RecipientFirstName = MyToString(reader.GetValue(11));
                        er.RecipientLastName = MyToString(reader.GetValue(12));
                        er.RecipientPostcode = MyToString(reader.GetValue(13));
                        er.RecipientState = MyToString(reader.GetValue(14));
                        er.RecipientEmail = MyToString(reader.GetValue(15));
                        er.RecipientDOB = MyToString(reader.GetValue(16));
                        er.RecipientGender = MyToString(reader.GetValue(17));
                        er.RecipientHealthFund = MyToString(reader.GetValue(18));
                        emailRecipientList.Add(er);
                    }

                    // Always call Close when done reading.
                    reader.Close();
                }

                connection.Close();

            }

            catch (Exception e)
            {
                LogWriter.Logger.Info(e.ToString());
            }

            return emailRecipientList;
        }

        private static string MyToString(object o)
        {
            if (o == DBNull.Value || o == null)
                return "";

            return o.ToString();
        }
    }
}

