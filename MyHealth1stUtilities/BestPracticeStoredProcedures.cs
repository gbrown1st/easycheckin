﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace MyHealth1stUtilities
{
    public class BestPracticeStoredProcedures
    {
        String connectionString;

        const string GET_ARRIVAL_PATIENT_ID = "SELECT dbo.PATIENTS.INTERNALID " +
        "FROM dbo.PATIENTS " +
        "INNER JOIN dbo.SEX ON dbo.PATIENTS.SEXCODE = dbo.SEX.SEXCODE " +
        "WHERE (UPPER(dbo.PATIENTS.FIRSTNAME) = @firstName) " +
        "AND (UPPER(dbo.PATIENTS.SURNAME) = @lastName) " +
        "AND (CONVERT(CHAR(10), dbo.PATIENTS.DOB, 126) = @dob) " +
        "AND (dbo.SEX.SEX = @gender)";

        const string GET_APPT_BY_MEDICARENO = @"SELECT dbo.APPOINTMENTS.RECORDID, dbo.APPOINTMENTS.USERID, dbo.PATIENTS.FIRSTNAME, 
        dbo.PATIENTS.SURNAME, dbo.PATIENTS.DOB, dbo.PATIENTS.MEDICARENO, 
        dbo.PATIENTS.ADDRESS1, dbo.PATIENTS.CITY, dbo.PATIENTS.POSTCODE, dbo.PATIENTS.MOBILEPHONE,
        (select count(a.RECORDID) from dbo.APPOINTMENTS a
        where a.USERID = dbo.APPOINTMENTS.USERID and a.APPOINTMENTCODE = 2
		AND (a.RECORDSTATUS = 1)
        AND Year(a.AppointmentDate) = Year(Getdate())
        AND Month(a.AppointmentDate) = Month(Getdate())
        AND Day(a.AppointmentDate) = Day(Getdate())) as NUMBERWAITING,
        (SELECT  Ltrim(Rtrim(dbo.titles.title)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.firstname)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.surname)) 
                 ) AS PRACTITIONER
        FROM dbo.APPOINTMENTS INNER JOIN
        dbo.PATIENTS ON dbo.APPOINTMENTS.INTERNALID = dbo.PATIENTS.INTERNALID INNER JOIN
        dbo.USERS ON dbo.APPOINTMENTS.USERID = dbo.USERS.USERID INNER JOIN
        dbo.TITLES ON dbo.USERS.TITLECODE = dbo.TITLES.TITLECODE
        WHERE (dbo.APPOINTMENTS.APPOINTMENTDATE = CONVERT(char(10), GETDATE(), 126))
        AND (dbo.APPOINTMENTS.RECORDSTATUS = 1) AND (dbo.PATIENTS.MEDICARENO = @medicareNumber)";

        const string GET_APPT_BY_GENDER_DOB = @"SELECT dbo.APPOINTMENTS.RECORDID, dbo.APPOINTMENTS.USERID, 
        dbo.PATIENTS.FIRSTNAME, dbo.PATIENTS.SURNAME, dbo.PATIENTS.DOB, dbo.PATIENTS.MEDICARENO, dbo.PATIENTS.ADDRESS1, 
        dbo.PATIENTS.CITY, dbo.PATIENTS.POSTCODE, dbo.PATIENTS.MOBILEPHONE,
        (SELECT COUNT(RECORDID) FROM dbo.APPOINTMENTS AS a
        WHERE (a.USERID = dbo.APPOINTMENTS.USERID) AND (a.APPOINTMENTCODE = 2)
		AND (a.RECORDSTATUS = 1)
        AND (YEAR(a.APPOINTMENTDATE) = YEAR(GETDATE())) 
        AND (MONTH(APPOINTMENTDATE) = MONTH(GETDATE())) 
        AND (DAY(a.APPOINTMENTDATE) = DAY(GETDATE()))) AS NUMBERWAITING,
        (SELECT  Ltrim(Rtrim(dbo.titles.title)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.firstname)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.surname)) 
                 ) AS PRACTITIONER
        FROM dbo.APPOINTMENTS INNER JOIN
        dbo.PATIENTS ON dbo.APPOINTMENTS.INTERNALID = dbo.PATIENTS.INTERNALID INNER JOIN
        dbo.SEX ON dbo.PATIENTS.SEXCODE = dbo.SEX.SEXCODE INNER JOIN
        dbo.USERS ON dbo.APPOINTMENTS.USERID = dbo.USERS.USERID INNER JOIN
        dbo.TITLES ON dbo.USERS.TITLECODE = dbo.TITLES.TITLECODE
        WHERE
		(dbo.APPOINTMENTS.APPOINTMENTDATE = CONVERT(char(10), GETDATE(), 126))
        AND (dbo.APPOINTMENTS.RECORDSTATUS = 1)
		AND (CONVERT(CHAR(10), dbo.PATIENTS.DOB, 126) = @dob)
        AND (dbo.SEX.SEX = @gender)";

        const string GET_APPOINTMENT = "SELECT RECORDID FROM APPOINTMENTS " +
        "WHERE INTERNALID = @patientID " +
        "AND APPOINTMENTDATE = CONVERT(char(10), GetDate(),126) " +
        "AND RECORDSTATUS = 1";
		
		const string GET_SMS_APPTS_LAST_5_MINS = @"SELECT dbo.PATIENTS.INTERNALID, dbo.PATIENTS.FIRSTNAME, dbo.PATIENTS.SURNAME, 
        dbo.PATIENTS.MOBILEPHONE, LEFT(CONVERT(VARCHAR, dbo.APPOINTMENTS.APPOINTMENTDATE,106), 11) AS APPOINTMENTDATE, dbo.APPOINTMENTS.APPOINTMENTTIME, 
        (SELECT  Ltrim(Rtrim(dbo.titles.title)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.firstname)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.surname)) 
                 ) AS PRACTITIONER,
			   (SELECT practicename FROM dbo.practice) AS PRACTICENAME 
        FROM dbo.APPOINTMENTS INNER JOIN
        dbo.PATIENTS ON dbo.APPOINTMENTS.INTERNALID = dbo.PATIENTS.INTERNALID INNER JOIN
        dbo.USERS ON dbo.APPOINTMENTS.USERID = dbo.USERS.USERID INNER JOIN
        dbo.TITLES ON dbo.USERS.TITLECODE = dbo.TITLES.TITLECODE
        WHERE dbo.APPOINTMENTS.CREATED <  GetDate() and dbo.APPOINTMENTS.CREATED > dateadd(minute, -@mins, GetDate())
		AND dbo.APPOINTMENTS.RECORDSTATUS = 1";
		
        const string GET_SMS_APPTS_HOURS = @"SELECT dbo.PATIENTS.INTERNALID, dbo.PATIENTS.FIRSTNAME, dbo.PATIENTS.SURNAME, 
        dbo.PATIENTS.MOBILEPHONE, LEFT(CONVERT(VARCHAR, dbo.APPOINTMENTS.APPOINTMENTDATE,106), 11) AS APPOINTMENTDATE, dbo.APPOINTMENTS.APPOINTMENTTIME, 
        (SELECT  Ltrim(Rtrim(dbo.titles.title)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.firstname)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.surname)) 
                 ) AS PRACTITIONER,
			   (SELECT practicename FROM dbo.practice) AS PRACTICENAME 
        FROM dbo.APPOINTMENTS INNER JOIN
        dbo.PATIENTS ON dbo.APPOINTMENTS.INTERNALID = dbo.PATIENTS.INTERNALID INNER JOIN
        dbo.USERS ON dbo.APPOINTMENTS.USERID = dbo.USERS.USERID INNER JOIN
        dbo.TITLES ON dbo.USERS.TITLECODE = dbo.TITLES.TITLECODE
        WHERE (dbo.APPOINTMENTS.APPOINTMENTDATE = CONVERT(char(10), GETDATE(), 126))
		AND (dbo.APPOINTMENTS.APPOINTMENTTIME >= DATEPART(SECOND, GETDATE()) + 60*DATEPART(MINUTE, GETDATE()) + 3600*DATEPART(HOUR, GETDATE())  + (@hours*3600))
		AND (dbo.APPOINTMENTS.APPOINTMENTTIME <= DATEPART(SECOND, GETDATE()) + 60*DATEPART(MINUTE, GETDATE()) + 3600*DATEPART(HOUR, GETDATE()) + (@hours*3600) + (@hours*3600))
		AND dbo.APPOINTMENTS.RECORDSTATUS = 1";

        const string GET_SMS_APPTS_DAYS = @"SELECT dbo.PATIENTS.INTERNALID, dbo.PATIENTS.FIRSTNAME, dbo.PATIENTS.SURNAME, 
        dbo.PATIENTS.MOBILEPHONE, LEFT(CONVERT(VARCHAR, dbo.APPOINTMENTS.APPOINTMENTDATE,106), 11) AS APPOINTMENTDATE, dbo.APPOINTMENTS.APPOINTMENTTIME, 
        (SELECT  Ltrim(Rtrim(dbo.titles.title)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.firstname)) + ' ' 
                 + Ltrim(Rtrim(dbo.users.surname)) 
                 ) AS PRACTITIONER,
			   (SELECT practicename FROM dbo.practice) AS PRACTICENAME 
        FROM dbo.APPOINTMENTS INNER JOIN
        dbo.PATIENTS ON dbo.APPOINTMENTS.INTERNALID = dbo.PATIENTS.INTERNALID INNER JOIN
        dbo.USERS ON dbo.APPOINTMENTS.USERID = dbo.USERS.USERID INNER JOIN
        dbo.TITLES ON dbo.USERS.TITLECODE = dbo.TITLES.TITLECODE
        WHERE (CAST(dbo.APPOINTMENTS.APPOINTMENTDATE AS DATE) = DATEADD(day, @days, CAST(GETDATE() AS DATE)))
		AND dbo.APPOINTMENTS.RECORDSTATUS = 1";

        const string GET_PATIENT_NAME_BY_ID = @"SELECT 
        FIRSTNAME, SURNAME
        FROM dbo.PATIENTS
        WHERE (INTERNALID = @patientID)";

        const string GET_WAITROOM_LIST = @"DECLARE @RC int
        EXECUTE @RC = [BPSPatients].[dbo].[BP_GetArrivedAppointments]"; 

        const string ADD_TO_WAITROOM = "DECLARE @RC int " +
        "DECLARE @aptid int " +
        "DECLARE @loginid int " +
        "EXECUTE @RC = [BPSPatients].[dbo].[BP_ArriveAppointment] " +
        "@apptID " +
        ",0";

        const string COUNT_YESTERDAYS_APPTS = "select COUNT(*) as number_of_appts, " +
        "'1stA' as appt_type from APPOINTMENTS " +
        "where REASON like '%1stA%' " +
        "and DATEDIFF(DAY, DATEADD(DAY, -1, CURRENT_TIMESTAMP), [APPOINTMENTDATE]) = 0 " +
        "UNION " +
        "select COUNT(*) as number_of_appts, " +
        "'PMS' as appt_type from APPOINTMENTS " +
        "where REASON not like '%1stA%' " +
        "and DATEDIFF(DAY, DATEADD(DAY, -1, CURRENT_TIMESTAMP), [APPOINTMENTDATE]) = 0";

        public BestPracticeStoredProcedures(String connectionString)
        {
            this.connectionString = connectionString;
        }

        private SqlConnection GetConnection()
        {
            var connection = new SqlConnection(this.connectionString);
            return connection;
        }

        public List<BestPracticeSMSReminder> getSMSAppointmentsLast5Mins(int mins)
        {
            var connection = new SqlConnection(this.connectionString);
            var smsList = new List<BestPracticeSMSReminder>();
            try
            {
                using (connection)
                {
                    var command = new SqlCommand(GET_SMS_APPTS_LAST_5_MINS, connection);
                    command.Parameters.AddWithValue("@mins", mins);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var smsReminder = new BestPracticeSMSReminder();
                        var patientID = (int)reader["INTERNALID"];
                        smsReminder.PATIENTID = patientID;
						int firstNameIndex = reader.GetOrdinal("FIRSTNAME"); 
                        var firstName = SafeGetString(reader, firstNameIndex);
                        firstName = firstName.Trim();
                        smsReminder.FIRSTNAME = firstName;
						int surnameIndex = reader.GetOrdinal("SURNAME"); 
                        var surname = SafeGetString(reader, surnameIndex);
                        surname = surname.Trim();
                        smsReminder.SURNAME = surname;
						int mobileIndex = reader.GetOrdinal("MOBILEPHONE"); 
                        var mobile = SafeGetString(reader, mobileIndex);
                        mobile = mobile.Trim();
                        smsReminder.MOBILEPHONE = mobile;
                        var apptDate = (string)reader["APPOINTMENTDATE"];
                        smsReminder.APPOINTMENTDATE = apptDate;
                        var appTime = (int)reader["APPOINTMENTTIME"];
                        smsReminder.APPOINTMENTTIME = appTime;
						int practitionerIndex = reader.GetOrdinal("PRACTITIONER"); 
                        var practitioner = SafeGetString(reader, practitionerIndex);
                        smsReminder.PRACTITIONER = practitioner;
						int practicenameIndex = reader.GetOrdinal("PRACTICENAME"); 
                        var practicename = SafeGetString(reader, practicenameIndex);
                        smsReminder.PRACTICENAME = practicename;
                        LogWriter.Logger.Info(smsReminder.ToString());
                        smsList.Add(smsReminder);
                    }

                    // Always call Close when done reading.
                    reader.Close();
                }

                connection.Close();

            }

            catch (Exception e)
            {
                LogWriter.Logger.Info(e.ToString());
            }

            return smsList;
        }
        public List<BestPracticeSMSReminder> getSMSAppointmentsHours(int hours)
        {
            var connection = new SqlConnection(this.connectionString);
            var smsList = new List<BestPracticeSMSReminder>();
            try
            {
                using (connection)
                {
                    var command = new SqlCommand(GET_SMS_APPTS_HOURS, connection);
                    command.Parameters.AddWithValue("@hours", hours);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var smsReminder = new BestPracticeSMSReminder();
                        var patientID = (int)reader["INTERNALID"];
                        smsReminder.PATIENTID = patientID;
						int firstNameIndex = reader.GetOrdinal("FIRSTNAME"); 
                        var firstName = SafeGetString(reader, firstNameIndex);
                        firstName = firstName.Trim();
                        smsReminder.FIRSTNAME = firstName;
						int surnameIndex = reader.GetOrdinal("SURNAME"); 
                        var surname = SafeGetString(reader, surnameIndex);
                        surname = surname.Trim();
                        smsReminder.SURNAME = surname;
						int mobileIndex = reader.GetOrdinal("MOBILEPHONE"); 
                        var mobile = SafeGetString(reader, mobileIndex);
                        mobile = mobile.Trim();
                        smsReminder.MOBILEPHONE = mobile;
                        var apptDate = (string)reader["APPOINTMENTDATE"];
                        smsReminder.APPOINTMENTDATE = apptDate;
                        var appTime = (int)reader["APPOINTMENTTIME"];
                        smsReminder.APPOINTMENTTIME = appTime;
						int practitionerIndex = reader.GetOrdinal("PRACTITIONER"); 
                        var practitioner = SafeGetString(reader, practitionerIndex);
                        smsReminder.PRACTITIONER = practitioner;
						int practicenameIndex = reader.GetOrdinal("PRACTICENAME"); 
                        var practicename = SafeGetString(reader, practicenameIndex);
                        smsReminder.PRACTICENAME = practicename;
                        LogWriter.Logger.Info(smsReminder.ToString());
                        smsList.Add(smsReminder);
                    }

                    // Always call Close when done reading.
                    reader.Close();
                }

                connection.Close();

            }

            catch (Exception e)
            {
                LogWriter.Logger.Info(e.ToString());
            }

            return smsList;
        }

        public List<BestPracticeSMSReminder> getSMSAppointmentsDays(int days)
        {
            var connection = new SqlConnection(this.connectionString);
            var smsList = new List<BestPracticeSMSReminder>();
            try
            {
                using (connection)
                {
                    var command = new SqlCommand(GET_SMS_APPTS_DAYS, connection);
                    command.Parameters.AddWithValue("@days", days);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var smsReminder = new BestPracticeSMSReminder();
                        var patientID = (int)reader["INTERNALID"];
                        smsReminder.PATIENTID = patientID;
						int firstNameIndex = reader.GetOrdinal("FIRSTNAME"); 
                        var firstName = SafeGetString(reader, firstNameIndex);
                        firstName = firstName.Trim();
                        smsReminder.FIRSTNAME = firstName;
						int surnameIndex = reader.GetOrdinal("SURNAME"); 
                        var surname = SafeGetString(reader, surnameIndex);
                        surname = surname.Trim();
                        smsReminder.SURNAME = surname;
						int mobileIndex = reader.GetOrdinal("MOBILEPHONE"); 
                        var mobile = SafeGetString(reader, mobileIndex);
                        mobile = mobile.Trim();
                        smsReminder.MOBILEPHONE = mobile;
                        int apptDateIndex = reader.GetOrdinal("APPOINTMENTDATE");
                        var apptDate = SafeGetString(reader, apptDateIndex);
                        apptDate = apptDate.Trim();
                        smsReminder.APPOINTMENTDATE = apptDate;
                        var appTime = (int)reader["APPOINTMENTTIME"];
                        smsReminder.APPOINTMENTTIME = appTime;
						int practitionerIndex = reader.GetOrdinal("PRACTITIONER"); 
                        var practitioner = SafeGetString(reader, practitionerIndex);
                        smsReminder.PRACTITIONER = practitioner;
						int practicenameIndex = reader.GetOrdinal("PRACTICENAME"); 
                        var practicename = SafeGetString(reader, practicenameIndex);
                        smsReminder.PRACTICENAME = practicename;
                        LogWriter.Logger.Info(smsReminder.ToString());
                        smsList.Add(smsReminder);
                    }

                    // Always call Close when done reading.
                    reader.Close();
                }

                connection.Close();

            }

            catch (Exception e)
            {
                LogWriter.Logger.Info(e.ToString());
            }

            return smsList;
        }
		
		public static string SafeGetString(SqlDataReader reader, int colIndex)
		{
		   if(!reader.IsDBNull(colIndex))
			   return reader.GetString(colIndex);
		   return string.Empty;
		}

        public List<BestPracticePatientAppointment> getAppointmentIDByMedicaredNumber(string medicareNumber)
        {
            LogWriter.Logger.Info("getAppointmentIDByMedicaredNumber = " + medicareNumber);
            var connection = new SqlConnection(this.connectionString);
            using (connection)
            {
                var command = new SqlCommand(GET_APPT_BY_MEDICARENO, connection);
                command.Parameters.AddWithValue("@medicareNumber", medicareNumber);
                var result = new List<BestPracticePatientAppointment>();
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        var patientAppt = new BestPracticePatientAppointment();
                        patientAppt.ApptID = (int)reader["RECORDID"];
                        patientAppt.UserID = (int)reader["USERID"];
                        patientAppt.numberWaiting = (int)reader["NUMBERWAITING"];
                        var firstName = (string)reader["FIRSTNAME"];
                        firstName = firstName.Trim();
                        patientAppt.FirstName = firstName;
                        var surname = (string)reader["SURNAME"];
                        surname = surname.Trim();
                        patientAppt.LastName = surname;
                        var DOB = Convert.ToDateTime(reader["DOB"]).ToString("dd/mm/yyyy");
                        patientAppt.DOB = DOB;
                        var mcn = (string)reader["MEDICARENO"];
                        mcn = mcn.Trim();
                        patientAppt.medicareno = mcn;
                        var address = (string)reader["ADDRESS1"];
                        address = address.Trim();
                        if (!String.IsNullOrEmpty(address) && address.IndexOf(' ') > -1)
                        {
                            address = HideCharacters(address);
                        }
                        patientAppt.address = address;
                        var city = (string)reader["CITY"];
                        city = city.Trim();
                        patientAppt.city = city;
                        var postcode = (string)reader["POSTCODE"];
                        postcode = postcode.Trim();
                        patientAppt.postcode = postcode;
                        var mobile = (string)reader["MOBILEPHONE"];
                        mobile = mobile.Trim();
                        if (mobile.Length > 3)
                        {
                            var last3 = mobile.Substring(mobile.Length - 3);
                            patientAppt.mobile = "*******" + last3;
                        }
                        else
                        {
                            patientAppt.mobile = mobile;
                        }
                        var practitioner = (string)reader["PRACTITIONER"];
                        practitioner = practitioner.Trim();
                        patientAppt.practitioner = practitioner;
                        LogWriter.Logger.Info(patientAppt.ToString());
                        result.Add(patientAppt);
                    } 
                    catch (Exception e) {
                        LogWriter.Logger.Info(e.ToString());
                    }
                }
                reader.Close();
                return result;

            }
        }

        public List<BestPracticePatientAppointment> getAppointmentIDByGenderDOB(string gender, string dob)
        {
            LogWriter.Logger.Info("getAppointmentIDByGenderDOB = " + gender + " " + dob);
            var connection = new SqlConnection(this.connectionString);
            using (connection)
            {
                var command = new SqlCommand(GET_APPT_BY_GENDER_DOB, connection);
                command.Parameters.AddWithValue("@gender", gender);
                command.Parameters.AddWithValue("@dob", dob);
                var result = new List<BestPracticePatientAppointment>();
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        var patientAppt = new BestPracticePatientAppointment();
                        patientAppt.ApptID = (int)reader["RECORDID"];
                        patientAppt.UserID = (int)reader["USERID"];
                        patientAppt.numberWaiting = (int)reader["NUMBERWAITING"];
                        var firstName = (string)reader["FIRSTNAME"];
                        firstName = firstName.Trim();
                        patientAppt.FirstName = firstName;
                        var surname = (string)reader["SURNAME"];
                        surname = surname.Trim();
                        patientAppt.LastName = surname;
                        var DOB = Convert.ToDateTime(reader["DOB"]).ToString("dd/mm/yyyy");
                        patientAppt.DOB = DOB;
                        var mcn = (string)reader["MEDICARENO"];
                        mcn = mcn.Trim();
                        patientAppt.medicareno = mcn;
                        var address = (string)reader["ADDRESS1"];
                        address = address.Trim();
                        if (!String.IsNullOrEmpty(address) && address.IndexOf(' ') > -1)
                        {
                            address = HideCharacters(address);
                        }
                        patientAppt.address = address;
                        var city = (string)reader["CITY"];
                        city = city.Trim();
                        patientAppt.city = city;
                        var postcode = (string)reader["POSTCODE"];
                        postcode = postcode.Trim();
                        patientAppt.postcode = postcode;
                        var mobile = (string)reader["MOBILEPHONE"];
                        mobile = mobile.Trim();
                        if (mobile.Length > 3) {
                            var last3 = mobile.Substring(mobile.Length - 3);
                            patientAppt.mobile = "*******" + last3;
                        } else {
                            patientAppt.mobile = mobile;
                        }
                        var practitioner = (string)reader["PRACTITIONER"];
                        practitioner = practitioner.Trim();
                        patientAppt.practitioner = practitioner;
                        LogWriter.Logger.Info(patientAppt.ToString());
                        result.Add(patientAppt);

                    }

                    catch (Exception e)
                    {
                        LogWriter.Logger.Info(e.ToString());
                    }

                }
                reader.Close();
                return result;

            }
        }

        public string HideCharacters (string str)
        {
            LogWriter.Logger.Info("HideCharacters = " + str);
            var strArray = str.Split(' ');
            var strToHide = strArray[1];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= strToHide.Length - 1; i++)
            {
                sb.AppendFormat("{0}", "*");
            }
            var returnStr = strArray[0] + " " + sb.ToString() + " " + strArray[2];
            return returnStr;
        }

        public string getPatientAppointmentByMedicareNumber(string medicareNumber)
        {
            LogWriter.Logger.Info("getPatientAppointmentByMedicareNumber = " + medicareNumber);
            SqlConnection connection = new SqlConnection(connectionString);

            string json = "";
            using (connection)
            {
                SqlCommand command = new SqlCommand(GET_APPT_BY_MEDICARENO, connection);
                command.Parameters.AddWithValue("@medicareNumber", medicareNumber);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }

        public int getPatientID(string firstName, string lastName, string dob, string gender)
        {

            var connection = new SqlConnection(this.connectionString);
            int patientID = 0;
            firstName = firstName.ToUpper();
            firstName = firstName.Trim();
            lastName = lastName.ToUpper();
            lastName = lastName.Trim();
            using (connection)
            {
                var command = new SqlCommand(GET_ARRIVAL_PATIENT_ID, connection);
                command.Parameters.AddWithValue("@firstName", firstName);
                command.Parameters.AddWithValue("@lastName", lastName);
                command.Parameters.AddWithValue("@dob", dob);
                command.Parameters.AddWithValue("@gender", gender);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    patientID = (int)reader["INTERNALID"];
                    return patientID;
                }
                reader.Close();
            }
            return patientID;
        }

        public int getAppointmentID(int patientID)
        {
            var connection = new SqlConnection(this.connectionString);
            var apptID = 0;
            using (connection)
            {
                var command = new SqlCommand(GET_APPOINTMENT, connection);
                command.Parameters.AddWithValue("@patientID", patientID);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    apptID = Convert.ToInt32(reader["RECORDID"]);
                }
                reader.Close();

            }
            return apptID;
        }

        public string getPatientName(int patientID)
        {
            var connection = new SqlConnection(this.connectionString);
            var patientName = "";
            using (connection)
            {
                var command = new SqlCommand(GET_PATIENT_NAME_BY_ID, connection);
                command.Parameters.AddWithValue("@patientID", patientID);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    patientName = (string)reader["FIRSTNAME"] + " " + (string)reader["SURNAME"];
                }
                reader.Close();

            }
            return patientName;
        }

        public int addToWaitroom(int apptID)
        {

            var connection = new SqlConnection(this.connectionString);
            int ret = 0;
            using (connection)
            {
                var command = new SqlCommand(ADD_TO_WAITROOM, connection);
                connection.Open();
                command.Parameters.AddWithValue("@apptID", apptID);
                ret = command.ExecuteNonQuery();
                return ret;
            }

        }

        public string countYesterdaysAppts()
        {
            var connection = new SqlConnection(this.connectionString);
            string json = "";
            using (connection)
            {
                var command = new SqlCommand(COUNT_YESTERDAYS_APPTS, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                json = Newtonsoft.Json.JsonConvert.SerializeObject(dataTable);
                reader.Close();

            }
            return json;
        }
        
    }

}
