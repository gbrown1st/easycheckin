﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;

namespace MyHealth1stUtilities
{
    public class LogWriter
    {
        private static LogWriter logWriter = new LogWriter();
        private EventLog log;
        private bool enabled = true;

        private LogWriter()
        {
            try
            {
                if (enabled)
                    this.log = new EventLog();
                if (log != null)
                    log.Source = "MyHealth1st Utilities";
            }
            catch (SecurityException)
            {
                enabled = false;
            }
        }

        public static LogWriter Logger
        {
            get
            {
                return logWriter;
            }
        }

        public void Error(string text)
        {
            try
            {
                if (enabled && log != null)
                    WriteToLog(text, EventLogEntryType.Error);
            }
            catch (Exception)
            {
            }
        }

        public void Info(string text)
        {
            try
            {
                if (enabled && log != null)
                    WriteToLog(text, EventLogEntryType.Information);
            }
            catch (Exception)
            {
            }
        }

        public void Warning(string text)
        {
            try
            {
                if (enabled && log != null)
                    WriteToLog(text, EventLogEntryType.Warning);
            }
            catch (Exception)
            {
            }
        }

        private void WriteToLog(string text, EventLogEntryType logEntryType)
        {
            log.WriteEntry(text, logEntryType);
        }
    }
}
