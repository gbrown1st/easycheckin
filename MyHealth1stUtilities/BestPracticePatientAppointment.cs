﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyHealth1stUtilities
{
    public class BestPracticePatientAppointment
    {
        public int ApptID { get; set; }
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string medicareno { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string mobile { get; set; }
        public int numberWaiting { get; set; }
        public string practitioner { get; set; }

        //dbo.PATIENTS.ADDRESS1, dbo.PATIENTS.CITY, dbo.PATIENTS.POSTCODE, dbo.PATIENTS.MOBILEPHONE

        public BestPracticePatientAppointment()
        {
        }

        override public string ToString()
        {
            return string.Format("ApptID: {0} - UserID: {1} - FirstName: {2} - LastName: {3} - DOB: {4} - Medicare No: {5} - Address: {6} - City: {7} - Postcode: {8} - Mobile: {9} - No. waiting: {10} - Practitioner: {11}", ApptID, UserID, FirstName, LastName, DOB, medicareno, address, city, postcode, mobile, numberWaiting, practitioner);
        }
    }

}