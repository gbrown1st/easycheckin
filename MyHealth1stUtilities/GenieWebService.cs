﻿using System;
using System.Configuration;

namespace MyHealth1stUtilities
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "GenieWebServiceBinding", Namespace = "http://www.geniesolutions.com.au/webservice")]
    public partial class GenieWebService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private System.Threading.SendOrPostCallback s44D_CloseConnectionOperationCompleted;

        private System.Threading.SendOrPostCallback s44D_NewConnectionOperationCompleted;

        private System.Threading.SendOrPostCallback WS_FindApptsOperationCompleted;

        private System.Threading.SendOrPostCallback WS_MakeApptOperationCompleted;

        private System.Threading.SendOrPostCallback WS_GetDrsOperationCompleted;

        private System.Threading.SendOrPostCallback WS_FindPatientsForDayOperationCompleted;

        private System.Threading.SendOrPostCallback WS_SearchAddressBookOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PlaceDocumentOperationCompleted;

        private System.Threading.SendOrPostCallback WS_GetTemplatesOperationCompleted;

        private System.Threading.SendOrPostCallback WS_FindByMedicareCardOperationCompleted;

        private System.Threading.SendOrPostCallback WS_MarkArrivedOperationCompleted;

        private System.Threading.SendOrPostCallback WS_SearchPatientsOperationCompleted;

        private System.Threading.SendOrPostCallback WS_SearchAB_ModOperationCompleted;

        private System.Threading.SendOrPostCallback WS_GetVersionOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_GetFreeApptsOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_MakeApptOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_GetTypesOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_CancelApptOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_GetTakenApptsOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_PatientAdminOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_NextAvailableOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_GetDrsOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_GetVacantApptsOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_MakeApptByTimeOperationCompleted;

        private System.Threading.SendOrPostCallback WS_PR_DeleteApptOperationCompleted;

        public GenieWebService()
        {
            Url = "http://localhost/4DSOAP/";
        }

        public GenieWebService(string url)
        {
            this.Url = url;
        }

        protected override System.Net.WebRequest GetWebRequest(Uri uri)
        {
            System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)base.GetWebRequest(uri);
            string keepAliveString = ConfigurationManager.AppSettings["genie.keepAlive"]; 
            bool keepAlive = keepAliveString.Equals("true");
            webRequest.KeepAlive = keepAlive;
            return webRequest;
        }

        /// <remarks/>
        public event s44D_CloseConnectionCompletedEventHandler s44D_CloseConnectionCompleted;

        /// <remarks/>
        public event s44D_NewConnectionCompletedEventHandler s44D_NewConnectionCompleted;

        /// <remarks/>
        public event WS_FindApptsCompletedEventHandler WS_FindApptsCompleted;

        /// <remarks/>
        public event WS_MakeApptCompletedEventHandler WS_MakeApptCompleted;

        /// <remarks/>
        public event WS_GetDrsCompletedEventHandler WS_GetDrsCompleted;

        /// <remarks/>
        public event WS_FindPatientsForDayCompletedEventHandler WS_FindPatientsForDayCompleted;

        /// <remarks/>
        public event WS_SearchAddressBookCompletedEventHandler WS_SearchAddressBookCompleted;

        /// <remarks/>
        public event WS_PlaceDocumentCompletedEventHandler WS_PlaceDocumentCompleted;

        /// <remarks/>
        public event WS_GetTemplatesCompletedEventHandler WS_GetTemplatesCompleted;

        /// <remarks/>
        public event WS_FindByMedicareCardCompletedEventHandler WS_FindByMedicareCardCompleted;

        /// <remarks/>
        public event WS_MarkArrivedCompletedEventHandler WS_MarkArrivedCompleted;

        /// <remarks/>
        public event WS_SearchPatientsCompletedEventHandler WS_SearchPatientsCompleted;

        /// <remarks/>
        public event WS_SearchAB_ModCompletedEventHandler WS_SearchAB_ModCompleted;

        /// <remarks/>
        public event WS_GetVersionCompletedEventHandler WS_GetVersionCompleted;

        /// <remarks/>
        public event WS_PR_GetFreeApptsCompletedEventHandler WS_PR_GetFreeApptsCompleted;

        /// <remarks/>
        public event WS_PR_MakeApptCompletedEventHandler WS_PR_MakeApptCompleted;

        /// <remarks/>
        public event WS_PR_GetTypesCompletedEventHandler WS_PR_GetTypesCompleted;

        /// <remarks/>
        public event WS_PR_CancelApptCompletedEventHandler WS_PR_CancelApptCompleted;

        /// <remarks/>
        public event WS_PR_GetTakenApptsCompletedEventHandler WS_PR_GetTakenApptsCompleted;

        /// <remarks/>
        public event WS_PR_PatientAdminCompletedEventHandler WS_PR_PatientAdminCompleted;

        /// <remarks/>
        public event WS_PR_NextAvailableCompletedEventHandler WS_PR_NextAvailableCompleted;

        /// <remarks/>
        public event WS_PR_GetDrsCompletedEventHandler WS_PR_GetDrsCompleted;

        /// <remarks/>
        public event WS_PR_GetVacantApptsCompletedEventHandler WS_PR_GetVacantApptsCompleted;

        /// <remarks/>
        public event WS_PR_MakeApptByTimeCompletedEventHandler WS_PR_MakeApptByTimeCompleted;

        /// <remarks/>
        public event WS_PR_DeleteApptCompletedEventHandler WS_PR_DeleteApptCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#s44D_CloseConnection", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int s44D_CloseConnection(string s44D_vConnectionID)
        {
            object[] results = this.Invoke("s44D_CloseConnection", new object[] {
                                               s44D_vConnectionID});
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult Begins44D_CloseConnection(string s44D_vConnectionID, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("s44D_CloseConnection", new object[] {
                                        s44D_vConnectionID}, callback, asyncState);
        }

        /// <remarks/>
        public int Ends44D_CloseConnection(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void s44D_CloseConnectionAsync(string s44D_vConnectionID)
        {
            this.s44D_CloseConnectionAsync(s44D_vConnectionID, null);
        }

        /// <remarks/>
        public void s44D_CloseConnectionAsync(string s44D_vConnectionID, object userState)
        {
            if ((this.s44D_CloseConnectionOperationCompleted == null))
            {
                this.s44D_CloseConnectionOperationCompleted = new System.Threading.SendOrPostCallback(this.Ons44D_CloseConnectionOperationCompleted);
            }
            this.InvokeAsync("s44D_CloseConnection", new object[] {
                                 s44D_vConnectionID}, this.s44D_CloseConnectionOperationCompleted, userState);
        }

        private void Ons44D_CloseConnectionOperationCompleted(object arg)
        {
            if ((this.s44D_CloseConnectionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.s44D_CloseConnectionCompleted(this, new s44D_CloseConnectionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#s44D_NewConnection", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int s44D_NewConnection(string s44D_vT_Sender, string s44D_vT_Password, out string s44D_vConnectionID)
        {
            object[] results = this.Invoke("s44D_NewConnection", new object[] {
                                               s44D_vT_Sender,
                                               s44D_vT_Password});
            s44D_vConnectionID = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult Begins44D_NewConnection(string s44D_vT_Sender, string s44D_vT_Password, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("s44D_NewConnection", new object[] {
                                        s44D_vT_Sender,
                                        s44D_vT_Password}, callback, asyncState);
        }

        /// <remarks/>
        public int Ends44D_NewConnection(System.IAsyncResult asyncResult, out string s44D_vConnectionID)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vConnectionID = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void s44D_NewConnectionAsync(string s44D_vT_Sender, string s44D_vT_Password)
        {
            this.s44D_NewConnectionAsync(s44D_vT_Sender, s44D_vT_Password, null);
        }

        /// <remarks/>
        public void s44D_NewConnectionAsync(string s44D_vT_Sender, string s44D_vT_Password, object userState)
        {
            if ((this.s44D_NewConnectionOperationCompleted == null))
            {
                this.s44D_NewConnectionOperationCompleted = new System.Threading.SendOrPostCallback(this.Ons44D_NewConnectionOperationCompleted);
            }
            this.InvokeAsync("s44D_NewConnection", new object[] {
                                 s44D_vT_Sender,
                                 s44D_vT_Password}, this.s44D_NewConnectionOperationCompleted, userState);
        }

        private void Ons44D_NewConnectionOperationCompleted(object arg)
        {
            if ((this.s44D_NewConnectionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.s44D_NewConnectionCompleted(this, new s44D_NewConnectionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_FindAppts", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_FindAppts(string s44D_vT_StartDate, string s44D_vT_EndDate, string s44D_vT_Type, int s44D_vL_Provider, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_FindAppts", new object[] {
                                               s44D_vT_StartDate,
                                               s44D_vT_EndDate,
                                               s44D_vT_Type,
                                               s44D_vL_Provider});
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_FindAppts(string s44D_vT_StartDate, string s44D_vT_EndDate, string s44D_vT_Type, int s44D_vL_Provider, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_FindAppts", new object[] {
                                        s44D_vT_StartDate,
                                        s44D_vT_EndDate,
                                        s44D_vT_Type,
                                        s44D_vL_Provider}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_FindAppts(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_FindApptsAsync(string s44D_vT_StartDate, string s44D_vT_EndDate, string s44D_vT_Type, int s44D_vL_Provider)
        {
            this.WS_FindApptsAsync(s44D_vT_StartDate, s44D_vT_EndDate, s44D_vT_Type, s44D_vL_Provider, null);
        }

        /// <remarks/>
        public void WS_FindApptsAsync(string s44D_vT_StartDate, string s44D_vT_EndDate, string s44D_vT_Type, int s44D_vL_Provider, object userState)
        {
            if ((this.WS_FindApptsOperationCompleted == null))
            {
                this.WS_FindApptsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_FindApptsOperationCompleted);
            }
            this.InvokeAsync("WS_FindAppts", new object[] {
                                 s44D_vT_StartDate,
                                 s44D_vT_EndDate,
                                 s44D_vT_Type,
                                 s44D_vL_Provider}, this.WS_FindApptsOperationCompleted, userState);
        }

        private void OnWS_FindApptsOperationCompleted(object arg)
        {
            if ((this.WS_FindApptsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_FindApptsCompleted(this, new WS_FindApptsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_MakeAppt", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_MakeAppt(int s44D_vL_ID, string s44D_vT_Name, string s44D_vT_Notes)
        {
            object[] results = this.Invoke("WS_MakeAppt", new object[] {
                                               s44D_vL_ID,
                                               s44D_vT_Name,
                                               s44D_vT_Notes});
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_MakeAppt(int s44D_vL_ID, string s44D_vT_Name, string s44D_vT_Notes, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_MakeAppt", new object[] {
                                        s44D_vL_ID,
                                        s44D_vT_Name,
                                        s44D_vT_Notes}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_MakeAppt(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_MakeApptAsync(int s44D_vL_ID, string s44D_vT_Name, string s44D_vT_Notes)
        {
            this.WS_MakeApptAsync(s44D_vL_ID, s44D_vT_Name, s44D_vT_Notes, null);
        }

        /// <remarks/>
        public void WS_MakeApptAsync(int s44D_vL_ID, string s44D_vT_Name, string s44D_vT_Notes, object userState)
        {
            if ((this.WS_MakeApptOperationCompleted == null))
            {
                this.WS_MakeApptOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_MakeApptOperationCompleted);
            }
            this.InvokeAsync("WS_MakeAppt", new object[] {
                                 s44D_vL_ID,
                                 s44D_vT_Name,
                                 s44D_vT_Notes}, this.WS_MakeApptOperationCompleted, userState);
        }

        private void OnWS_MakeApptOperationCompleted(object arg)
        {
            if ((this.WS_MakeApptCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_MakeApptCompleted(this, new WS_MakeApptCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_GetDrs", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_GetDrs([System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_GetDrs", new object[0]);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_GetDrs(System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_GetDrs", new object[0], callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_GetDrs(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_GetDrsAsync()
        {
            this.WS_GetDrsAsync(null);
        }

        /// <remarks/>
        public void WS_GetDrsAsync(object userState)
        {
            if ((this.WS_GetDrsOperationCompleted == null))
            {
                this.WS_GetDrsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_GetDrsOperationCompleted);
            }
            this.InvokeAsync("WS_GetDrs", new object[0], this.WS_GetDrsOperationCompleted, userState);
        }

        private void OnWS_GetDrsOperationCompleted(object arg)
        {
            if ((this.WS_GetDrsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_GetDrsCompleted(this, new WS_GetDrsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_FindPatientsForDay", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_FindPatientsForDay(string s44D_vT_StartDate, out string s44D_vT_Output)
        {
            object[] results = this.Invoke("WS_FindPatientsForDay", new object[] {
                                               s44D_vT_StartDate});
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_FindPatientsForDay(string s44D_vT_StartDate, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_FindPatientsForDay", new object[] {
                                        s44D_vT_StartDate}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_FindPatientsForDay(System.IAsyncResult asyncResult, out string s44D_vT_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_FindPatientsForDayAsync(string s44D_vT_StartDate)
        {
            this.WS_FindPatientsForDayAsync(s44D_vT_StartDate, null);
        }

        /// <remarks/>
        public void WS_FindPatientsForDayAsync(string s44D_vT_StartDate, object userState)
        {
            if ((this.WS_FindPatientsForDayOperationCompleted == null))
            {
                this.WS_FindPatientsForDayOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_FindPatientsForDayOperationCompleted);
            }
            this.InvokeAsync("WS_FindPatientsForDay", new object[] {
                                 s44D_vT_StartDate}, this.WS_FindPatientsForDayOperationCompleted, userState);
        }

        private void OnWS_FindPatientsForDayOperationCompleted(object arg)
        {
            if ((this.WS_FindPatientsForDayCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_FindPatientsForDayCompleted(this, new WS_FindPatientsForDayCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_SearchAddressBook", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_SearchAddressBook(string s44D_vT_AddresseeSurname, string s44D_vT_First, out string s44D_vT_Output)
        {
            object[] results = this.Invoke("WS_SearchAddressBook", new object[] {
                                               s44D_vT_AddresseeSurname,
                                               s44D_vT_First});
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_SearchAddressBook(string s44D_vT_AddresseeSurname, string s44D_vT_First, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_SearchAddressBook", new object[] {
                                        s44D_vT_AddresseeSurname,
                                        s44D_vT_First}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_SearchAddressBook(System.IAsyncResult asyncResult, out string s44D_vT_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_SearchAddressBookAsync(string s44D_vT_AddresseeSurname, string s44D_vT_First)
        {
            this.WS_SearchAddressBookAsync(s44D_vT_AddresseeSurname, s44D_vT_First, null);
        }

        /// <remarks/>
        public void WS_SearchAddressBookAsync(string s44D_vT_AddresseeSurname, string s44D_vT_First, object userState)
        {
            if ((this.WS_SearchAddressBookOperationCompleted == null))
            {
                this.WS_SearchAddressBookOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_SearchAddressBookOperationCompleted);
            }
            this.InvokeAsync("WS_SearchAddressBook", new object[] {
                                 s44D_vT_AddresseeSurname,
                                 s44D_vT_First}, this.WS_SearchAddressBookOperationCompleted, userState);
        }

        private void OnWS_SearchAddressBookOperationCompleted(object arg)
        {
            if ((this.WS_SearchAddressBookCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_SearchAddressBookCompleted(this, new WS_SearchAddressBookCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PlaceDocument", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PlaceDocument(string s44D_vT_Output)
        {
            object[] results = this.Invoke("WS_PlaceDocument", new object[] {
                                               s44D_vT_Output});
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PlaceDocument(string s44D_vT_Output, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PlaceDocument", new object[] {
                                        s44D_vT_Output}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PlaceDocument(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PlaceDocumentAsync(string s44D_vT_Output)
        {
            this.WS_PlaceDocumentAsync(s44D_vT_Output, null);
        }

        /// <remarks/>
        public void WS_PlaceDocumentAsync(string s44D_vT_Output, object userState)
        {
            if ((this.WS_PlaceDocumentOperationCompleted == null))
            {
                this.WS_PlaceDocumentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PlaceDocumentOperationCompleted);
            }
            this.InvokeAsync("WS_PlaceDocument", new object[] {
                                 s44D_vT_Output}, this.WS_PlaceDocumentOperationCompleted, userState);
        }

        private void OnWS_PlaceDocumentOperationCompleted(object arg)
        {
            if ((this.WS_PlaceDocumentCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PlaceDocumentCompleted(this, new WS_PlaceDocumentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_GetTemplates", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_GetTemplates(out string s44D_vT_Output)
        {
            object[] results = this.Invoke("WS_GetTemplates", new object[0]);
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_GetTemplates(System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_GetTemplates", new object[0], callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_GetTemplates(System.IAsyncResult asyncResult, out string s44D_vT_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_GetTemplatesAsync()
        {
            this.WS_GetTemplatesAsync(null);
        }

        /// <remarks/>
        public void WS_GetTemplatesAsync(object userState)
        {
            if ((this.WS_GetTemplatesOperationCompleted == null))
            {
                this.WS_GetTemplatesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_GetTemplatesOperationCompleted);
            }
            this.InvokeAsync("WS_GetTemplates", new object[0], this.WS_GetTemplatesOperationCompleted, userState);
        }

        private void OnWS_GetTemplatesOperationCompleted(object arg)
        {
            if ((this.WS_GetTemplatesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_GetTemplatesCompleted(this, new WS_GetTemplatesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_FindByMedicareCard", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_FindByMedicareCard(string vT_Medicare, string s44D_vT_StartDate, out string s44D_vT_Output)
        {
            object[] results = this.Invoke("WS_FindByMedicareCard", new object[] {
                                               vT_Medicare,
                                               s44D_vT_StartDate});
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_FindByMedicareCard(string vT_Medicare, string s44D_vT_StartDate, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_FindByMedicareCard", new object[] {
                                        vT_Medicare,
                                        s44D_vT_StartDate}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_FindByMedicareCard(System.IAsyncResult asyncResult, out string s44D_vT_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_FindByMedicareCardAsync(string vT_Medicare, string s44D_vT_StartDate)
        {
            this.WS_FindByMedicareCardAsync(vT_Medicare, s44D_vT_StartDate, null);
        }

        /// <remarks/>
        public void WS_FindByMedicareCardAsync(string vT_Medicare, string s44D_vT_StartDate, object userState)
        {
            if ((this.WS_FindByMedicareCardOperationCompleted == null))
            {
                this.WS_FindByMedicareCardOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_FindByMedicareCardOperationCompleted);
            }
            this.InvokeAsync("WS_FindByMedicareCard", new object[] {
                                 vT_Medicare,
                                 s44D_vT_StartDate}, this.WS_FindByMedicareCardOperationCompleted, userState);
        }

        private void OnWS_FindByMedicareCardOperationCompleted(object arg)
        {
            if ((this.WS_FindByMedicareCardCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_FindByMedicareCardCompleted(this, new WS_FindByMedicareCardCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_MarkArrived", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_MarkArrived(int s44D_vL_ID)
        {
            object[] results = this.Invoke("WS_MarkArrived", new object[] {
                                               s44D_vL_ID});
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_MarkArrived(int s44D_vL_ID, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_MarkArrived", new object[] {
                                        s44D_vL_ID}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_MarkArrived(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_MarkArrivedAsync(int s44D_vL_ID)
        {
            this.WS_MarkArrivedAsync(s44D_vL_ID, null);
        }

        /// <remarks/>
        public void WS_MarkArrivedAsync(int s44D_vL_ID, object userState)
        {
            if ((this.WS_MarkArrivedOperationCompleted == null))
            {
                this.WS_MarkArrivedOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_MarkArrivedOperationCompleted);
            }
            this.InvokeAsync("WS_MarkArrived", new object[] {
                                 s44D_vL_ID}, this.WS_MarkArrivedOperationCompleted, userState);
        }

        private void OnWS_MarkArrivedOperationCompleted(object arg)
        {
            if ((this.WS_MarkArrivedCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_MarkArrivedCompleted(this, new WS_MarkArrivedCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_SearchPatients", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_SearchPatients(string s44D_vT_Surname, string s44D_vT_First, out string s44D_vT_Output)
        {
            object[] results = this.Invoke("WS_SearchPatients", new object[] {
                                               s44D_vT_Surname,
                                               s44D_vT_First});
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_SearchPatients(string s44D_vT_Surname, string s44D_vT_First, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_SearchPatients", new object[] {
                                        s44D_vT_Surname,
                                        s44D_vT_First}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_SearchPatients(System.IAsyncResult asyncResult, out string s44D_vT_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_SearchPatientsAsync(string s44D_vT_Surname, string s44D_vT_First)
        {
            this.WS_SearchPatientsAsync(s44D_vT_Surname, s44D_vT_First, null);
        }

        /// <remarks/>
        public void WS_SearchPatientsAsync(string s44D_vT_Surname, string s44D_vT_First, object userState)
        {
            if ((this.WS_SearchPatientsOperationCompleted == null))
            {
                this.WS_SearchPatientsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_SearchPatientsOperationCompleted);
            }
            this.InvokeAsync("WS_SearchPatients", new object[] {
                                 s44D_vT_Surname,
                                 s44D_vT_First}, this.WS_SearchPatientsOperationCompleted, userState);
        }

        private void OnWS_SearchPatientsOperationCompleted(object arg)
        {
            if ((this.WS_SearchPatientsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_SearchPatientsCompleted(this, new WS_SearchPatientsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_SearchAB_Mod", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_SearchAB_Mod(string s44D_vT_AddresseeSurname, string s44D_vT_First, string s44D_vT_DateTimeStamp, out string s44D_vT_Output)
        {
            object[] results = this.Invoke("WS_SearchAB_Mod", new object[] {
                                               s44D_vT_AddresseeSurname,
                                               s44D_vT_First,
                                               s44D_vT_DateTimeStamp});
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_SearchAB_Mod(string s44D_vT_AddresseeSurname, string s44D_vT_First, string s44D_vT_DateTimeStamp, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_SearchAB_Mod", new object[] {
                                        s44D_vT_AddresseeSurname,
                                        s44D_vT_First,
                                        s44D_vT_DateTimeStamp}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_SearchAB_Mod(System.IAsyncResult asyncResult, out string s44D_vT_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_Output = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_SearchAB_ModAsync(string s44D_vT_AddresseeSurname, string s44D_vT_First, string s44D_vT_DateTimeStamp)
        {
            this.WS_SearchAB_ModAsync(s44D_vT_AddresseeSurname, s44D_vT_First, s44D_vT_DateTimeStamp, null);
        }

        /// <remarks/>
        public void WS_SearchAB_ModAsync(string s44D_vT_AddresseeSurname, string s44D_vT_First, string s44D_vT_DateTimeStamp, object userState)
        {
            if ((this.WS_SearchAB_ModOperationCompleted == null))
            {
                this.WS_SearchAB_ModOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_SearchAB_ModOperationCompleted);
            }
            this.InvokeAsync("WS_SearchAB_Mod", new object[] {
                                 s44D_vT_AddresseeSurname,
                                 s44D_vT_First,
                                 s44D_vT_DateTimeStamp}, this.WS_SearchAB_ModOperationCompleted, userState);
        }

        private void OnWS_SearchAB_ModOperationCompleted(object arg)
        {
            if ((this.WS_SearchAB_ModCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_SearchAB_ModCompleted(this, new WS_SearchAB_ModCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_GetVersion", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("vR_Version")]
        public float WS_GetVersion()
        {
            object[] results = this.Invoke("WS_GetVersion", new object[0]);
            return ((float)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_GetVersion(System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_GetVersion", new object[0], callback, asyncState);
        }

        /// <remarks/>
        public float EndWS_GetVersion(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((float)(results[0]));
        }

        /// <remarks/>
        public void WS_GetVersionAsync()
        {
            this.WS_GetVersionAsync(null);
        }

        /// <remarks/>
        public void WS_GetVersionAsync(object userState)
        {
            if ((this.WS_GetVersionOperationCompleted == null))
            {
                this.WS_GetVersionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_GetVersionOperationCompleted);
            }
            this.InvokeAsync("WS_GetVersion", new object[0], this.WS_GetVersionOperationCompleted, userState);
        }

        private void OnWS_GetVersionOperationCompleted(object arg)
        {
            if ((this.WS_GetVersionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_GetVersionCompleted(this, new WS_GetVersionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_GetFreeAppts", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_GetFreeAppts(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_PR_GetFreeAppts", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_StartDate,
                                               s44D_vT_EndDate,
                                               s44D_vL_Provider});
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_GetFreeAppts(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_GetFreeAppts", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_StartDate,
                                        s44D_vT_EndDate,
                                        s44D_vL_Provider}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_GetFreeAppts(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_GetFreeApptsAsync(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider)
        {
            this.WS_PR_GetFreeApptsAsync(s44D_vConnectionID, s44D_vT_StartDate, s44D_vT_EndDate, s44D_vL_Provider, null);
        }

        /// <remarks/>
        public void WS_PR_GetFreeApptsAsync(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, object userState)
        {
            if ((this.WS_PR_GetFreeApptsOperationCompleted == null))
            {
                this.WS_PR_GetFreeApptsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_GetFreeApptsOperationCompleted);
            }
            this.InvokeAsync("WS_PR_GetFreeAppts", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_StartDate,
                                 s44D_vT_EndDate,
                                 s44D_vL_Provider}, this.WS_PR_GetFreeApptsOperationCompleted, userState);
        }

        private void OnWS_PR_GetFreeApptsOperationCompleted(object arg)
        {
            if ((this.WS_PR_GetFreeApptsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_GetFreeApptsCompleted(this, new WS_PR_GetFreeApptsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_MakeAppt", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_MakeAppt(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes, out string s44D_vT_PtCreated)
        {
            object[] results = this.Invoke("WS_PR_MakeAppt", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_Sender,
                                               s44D_vL_ID,
                                               s44D_vT_FirstName,
                                               s44D_vT_Surname,
                                               s44D_vT_DOB,
                                               s44D_vT_Code,
                                               s44D_vT_Mobile,
                                               s44D_vT_Email,
                                               s44D_vT_Notes});
            s44D_vT_PtCreated = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_MakeAppt(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_MakeAppt", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_Sender,
                                        s44D_vL_ID,
                                        s44D_vT_FirstName,
                                        s44D_vT_Surname,
                                        s44D_vT_DOB,
                                        s44D_vT_Code,
                                        s44D_vT_Mobile,
                                        s44D_vT_Email,
                                        s44D_vT_Notes}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_MakeAppt(System.IAsyncResult asyncResult, out string s44D_vT_PtCreated)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_PtCreated = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_MakeApptAsync(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes)
        {
            this.WS_PR_MakeApptAsync(s44D_vConnectionID, s44D_vT_Sender, s44D_vL_ID, s44D_vT_FirstName, s44D_vT_Surname, s44D_vT_DOB, s44D_vT_Code, s44D_vT_Mobile, s44D_vT_Email, s44D_vT_Notes, null);
        }

        /// <remarks/>
        public void WS_PR_MakeApptAsync(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes, object userState)
        {
            if ((this.WS_PR_MakeApptOperationCompleted == null))
            {
                this.WS_PR_MakeApptOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_MakeApptOperationCompleted);
            }
            this.InvokeAsync("WS_PR_MakeAppt", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_Sender,
                                 s44D_vL_ID,
                                 s44D_vT_FirstName,
                                 s44D_vT_Surname,
                                 s44D_vT_DOB,
                                 s44D_vT_Code,
                                 s44D_vT_Mobile,
                                 s44D_vT_Email,
                                 s44D_vT_Notes}, this.WS_PR_MakeApptOperationCompleted, userState);
        }

        private void OnWS_PR_MakeApptOperationCompleted(object arg)
        {
            if ((this.WS_PR_MakeApptCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_MakeApptCompleted(this, new WS_PR_MakeApptCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_GetTypes", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_GetTypes(string s44D_vConnectionID, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_PR_GetTypes", new object[] {
                                               s44D_vConnectionID});
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_GetTypes(string s44D_vConnectionID, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_GetTypes", new object[] {
                                        s44D_vConnectionID}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_GetTypes(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_GetTypesAsync(string s44D_vConnectionID)
        {
            this.WS_PR_GetTypesAsync(s44D_vConnectionID, null);
        }

        /// <remarks/>
        public void WS_PR_GetTypesAsync(string s44D_vConnectionID, object userState)
        {
            if ((this.WS_PR_GetTypesOperationCompleted == null))
            {
                this.WS_PR_GetTypesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_GetTypesOperationCompleted);
            }
            this.InvokeAsync("WS_PR_GetTypes", new object[] {
                                 s44D_vConnectionID}, this.WS_PR_GetTypesOperationCompleted, userState);
        }

        private void OnWS_PR_GetTypesOperationCompleted(object arg)
        {
            if ((this.WS_PR_GetTypesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_GetTypesCompleted(this, new WS_PR_GetTypesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_CancelAppt", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_CancelAppt(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID)
        {
            object[] results = this.Invoke("WS_PR_CancelAppt", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_Sender,
                                               s44D_vL_ID});
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_CancelAppt(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_CancelAppt", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_Sender,
                                        s44D_vL_ID}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_CancelAppt(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_CancelApptAsync(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID)
        {
            this.WS_PR_CancelApptAsync(s44D_vConnectionID, s44D_vT_Sender, s44D_vL_ID, null);
        }

        /// <remarks/>
        public void WS_PR_CancelApptAsync(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, object userState)
        {
            if ((this.WS_PR_CancelApptOperationCompleted == null))
            {
                this.WS_PR_CancelApptOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_CancelApptOperationCompleted);
            }
            this.InvokeAsync("WS_PR_CancelAppt", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_Sender,
                                 s44D_vL_ID}, this.WS_PR_CancelApptOperationCompleted, userState);
        }

        private void OnWS_PR_CancelApptOperationCompleted(object arg)
        {
            if ((this.WS_PR_CancelApptCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_CancelApptCompleted(this, new WS_PR_CancelApptCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_GetTakenAppts", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_GetTakenAppts(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_PR_GetTakenAppts", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_Sender,
                                               s44D_vT_StartDate,
                                               s44D_vT_EndDate,
                                               s44D_vL_Provider});
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_GetTakenAppts(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_GetTakenAppts", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_Sender,
                                        s44D_vT_StartDate,
                                        s44D_vT_EndDate,
                                        s44D_vL_Provider}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_GetTakenAppts(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_GetTakenApptsAsync(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider)
        {
            this.WS_PR_GetTakenApptsAsync(s44D_vConnectionID, s44D_vT_Sender, s44D_vT_StartDate, s44D_vT_EndDate, s44D_vL_Provider, null);
        }

        /// <remarks/>
        public void WS_PR_GetTakenApptsAsync(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, object userState)
        {
            if ((this.WS_PR_GetTakenApptsOperationCompleted == null))
            {
                this.WS_PR_GetTakenApptsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_GetTakenApptsOperationCompleted);
            }
            this.InvokeAsync("WS_PR_GetTakenAppts", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_Sender,
                                 s44D_vT_StartDate,
                                 s44D_vT_EndDate,
                                 s44D_vL_Provider}, this.WS_PR_GetTakenApptsOperationCompleted, userState);
        }

        private void OnWS_PR_GetTakenApptsOperationCompleted(object arg)
        {
            if ((this.WS_PR_GetTakenApptsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_GetTakenApptsCompleted(this, new WS_PR_GetTakenApptsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_PatientAdmin", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_PatientAdmin(string s44D_vConnectionID, string s44D_vT_Sender, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] byte[] s44D_vX_Input)
        {
            object[] results = this.Invoke("WS_PR_PatientAdmin", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_Sender,
                                               s44D_vX_Input});
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_PatientAdmin(string s44D_vConnectionID, string s44D_vT_Sender, byte[] s44D_vX_Input, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_PatientAdmin", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_Sender,
                                        s44D_vX_Input}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_PatientAdmin(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_PatientAdminAsync(string s44D_vConnectionID, string s44D_vT_Sender, byte[] s44D_vX_Input)
        {
            this.WS_PR_PatientAdminAsync(s44D_vConnectionID, s44D_vT_Sender, s44D_vX_Input, null);
        }

        /// <remarks/>
        public void WS_PR_PatientAdminAsync(string s44D_vConnectionID, string s44D_vT_Sender, byte[] s44D_vX_Input, object userState)
        {
            if ((this.WS_PR_PatientAdminOperationCompleted == null))
            {
                this.WS_PR_PatientAdminOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_PatientAdminOperationCompleted);
            }
            this.InvokeAsync("WS_PR_PatientAdmin", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_Sender,
                                 s44D_vX_Input}, this.WS_PR_PatientAdminOperationCompleted, userState);
        }

        private void OnWS_PR_PatientAdminOperationCompleted(object arg)
        {
            if ((this.WS_PR_PatientAdminCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_PatientAdminCompleted(this, new WS_PR_PatientAdminCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_NextAvailable", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_NextAvailable(string s44D_vConnectionID, int s44D_vL_Provider, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_PR_NextAvailable", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vL_Provider});
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_NextAvailable(string s44D_vConnectionID, int s44D_vL_Provider, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_NextAvailable", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vL_Provider}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_NextAvailable(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_NextAvailableAsync(string s44D_vConnectionID, int s44D_vL_Provider)
        {
            this.WS_PR_NextAvailableAsync(s44D_vConnectionID, s44D_vL_Provider, null);
        }

        /// <remarks/>
        public void WS_PR_NextAvailableAsync(string s44D_vConnectionID, int s44D_vL_Provider, object userState)
        {
            if ((this.WS_PR_NextAvailableOperationCompleted == null))
            {
                this.WS_PR_NextAvailableOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_NextAvailableOperationCompleted);
            }
            this.InvokeAsync("WS_PR_NextAvailable", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vL_Provider}, this.WS_PR_NextAvailableOperationCompleted, userState);
        }

        private void OnWS_PR_NextAvailableOperationCompleted(object arg)
        {
            if ((this.WS_PR_NextAvailableCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_NextAvailableCompleted(this, new WS_PR_NextAvailableCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_GetDrs", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_GetDrs(string s44D_vConnectionID, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_PR_GetDrs", new object[] {
                                               s44D_vConnectionID});
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_GetDrs(string s44D_vConnectionID, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_GetDrs", new object[] {
                                        s44D_vConnectionID}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_GetDrs(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_GetDrsAsync(string s44D_vConnectionID)
        {
            this.WS_PR_GetDrsAsync(s44D_vConnectionID, null);
        }

        /// <remarks/>
        public void WS_PR_GetDrsAsync(string s44D_vConnectionID, object userState)
        {
            if ((this.WS_PR_GetDrsOperationCompleted == null))
            {
                this.WS_PR_GetDrsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_GetDrsOperationCompleted);
            }
            this.InvokeAsync("WS_PR_GetDrs", new object[] {
                                 s44D_vConnectionID}, this.WS_PR_GetDrsOperationCompleted, userState);
        }

        private void OnWS_PR_GetDrsOperationCompleted(object arg)
        {
            if ((this.WS_PR_GetDrsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_GetDrsCompleted(this, new WS_PR_GetDrsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_GetVacantAppts", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_GetVacantAppts(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")] out byte[] s44D_vX_Output)
        {
            object[] results = this.Invoke("WS_PR_GetVacantAppts", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_StartDate,
                                               s44D_vT_EndDate,
                                               s44D_vL_Provider});
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_GetVacantAppts(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_GetVacantAppts", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_StartDate,
                                        s44D_vT_EndDate,
                                        s44D_vL_Provider}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_GetVacantAppts(System.IAsyncResult asyncResult, out byte[] s44D_vX_Output)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vX_Output = ((byte[])(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_GetVacantApptsAsync(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider)
        {
            this.WS_PR_GetVacantApptsAsync(s44D_vConnectionID, s44D_vT_StartDate, s44D_vT_EndDate, s44D_vL_Provider, null);
        }

        /// <remarks/>
        public void WS_PR_GetVacantApptsAsync(string s44D_vConnectionID, string s44D_vT_StartDate, string s44D_vT_EndDate, int s44D_vL_Provider, object userState)
        {
            if ((this.WS_PR_GetVacantApptsOperationCompleted == null))
            {
                this.WS_PR_GetVacantApptsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_GetVacantApptsOperationCompleted);
            }
            this.InvokeAsync("WS_PR_GetVacantAppts", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_StartDate,
                                 s44D_vT_EndDate,
                                 s44D_vL_Provider}, this.WS_PR_GetVacantApptsOperationCompleted, userState);
        }

        private void OnWS_PR_GetVacantApptsOperationCompleted(object arg)
        {
            if ((this.WS_PR_GetVacantApptsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_GetVacantApptsCompleted(this, new WS_PR_GetVacantApptsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_MakeApptByTime", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_MakeApptByTime(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, int s44D_vL_Duration, int s44D_vL_Provider, string s44D_vT_Type, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes, out string s44D_vT_PtCreated)
        {
            object[] results = this.Invoke("WS_PR_MakeApptByTime", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_Sender,
                                               s44D_vT_StartDate,
                                               s44D_vL_Duration,
                                               s44D_vL_Provider,
                                               s44D_vT_Type,
                                               s44D_vT_FirstName,
                                               s44D_vT_Surname,
                                               s44D_vT_DOB,
                                               s44D_vT_Code,
                                               s44D_vT_Mobile,
                                               s44D_vT_Email,
                                               s44D_vT_Notes});
            s44D_vT_PtCreated = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_MakeApptByTime(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, int s44D_vL_Duration, int s44D_vL_Provider, string s44D_vT_Type, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_MakeApptByTime", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_Sender,
                                        s44D_vT_StartDate,
                                        s44D_vL_Duration,
                                        s44D_vL_Provider,
                                        s44D_vT_Type,
                                        s44D_vT_FirstName,
                                        s44D_vT_Surname,
                                        s44D_vT_DOB,
                                        s44D_vT_Code,
                                        s44D_vT_Mobile,
                                        s44D_vT_Email,
                                        s44D_vT_Notes}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_MakeApptByTime(System.IAsyncResult asyncResult, out string s44D_vT_PtCreated)
        {
            object[] results = this.EndInvoke(asyncResult);
            s44D_vT_PtCreated = ((string)(results[1]));
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_MakeApptByTimeAsync(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, int s44D_vL_Duration, int s44D_vL_Provider, string s44D_vT_Type, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes)
        {
            this.WS_PR_MakeApptByTimeAsync(s44D_vConnectionID, s44D_vT_Sender, s44D_vT_StartDate, s44D_vL_Duration, s44D_vL_Provider, s44D_vT_Type, s44D_vT_FirstName, s44D_vT_Surname, s44D_vT_DOB, s44D_vT_Code, s44D_vT_Mobile, s44D_vT_Email, s44D_vT_Notes, null);
        }

        /// <remarks/>
        public void WS_PR_MakeApptByTimeAsync(string s44D_vConnectionID, string s44D_vT_Sender, string s44D_vT_StartDate, int s44D_vL_Duration, int s44D_vL_Provider, string s44D_vT_Type, string s44D_vT_FirstName, string s44D_vT_Surname, string s44D_vT_DOB, string s44D_vT_Code, string s44D_vT_Mobile, string s44D_vT_Email, string s44D_vT_Notes, object userState)
        {
            if ((this.WS_PR_MakeApptByTimeOperationCompleted == null))
            {
                this.WS_PR_MakeApptByTimeOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_MakeApptByTimeOperationCompleted);
            }
            this.InvokeAsync("WS_PR_MakeApptByTime", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_Sender,
                                 s44D_vT_StartDate,
                                 s44D_vL_Duration,
                                 s44D_vL_Provider,
                                 s44D_vT_Type,
                                 s44D_vT_FirstName,
                                 s44D_vT_Surname,
                                 s44D_vT_DOB,
                                 s44D_vT_Code,
                                 s44D_vT_Mobile,
                                 s44D_vT_Email,
                                 s44D_vT_Notes}, this.WS_PR_MakeApptByTimeOperationCompleted, userState);
        }

        private void OnWS_PR_MakeApptByTimeOperationCompleted(object arg)
        {
            if ((this.WS_PR_MakeApptByTimeCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_MakeApptByTimeCompleted(this, new WS_PR_MakeApptByTimeCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("GenieWebService#WS_PR_DeleteAppt", RequestNamespace = "http://www.geniesolutions.com.au/webservice", ResponseNamespace = "http://www.geniesolutions.com.au/webservice")]
        [return: System.Xml.Serialization.SoapElementAttribute("s44D_vL_Error")]
        public int WS_PR_DeleteAppt(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID)
        {
            object[] results = this.Invoke("WS_PR_DeleteAppt", new object[] {
                                               s44D_vConnectionID,
                                               s44D_vT_Sender,
                                               s44D_vL_ID});
            return ((int)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginWS_PR_DeleteAppt(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("WS_PR_DeleteAppt", new object[] {
                                        s44D_vConnectionID,
                                        s44D_vT_Sender,
                                        s44D_vL_ID}, callback, asyncState);
        }

        /// <remarks/>
        public int EndWS_PR_DeleteAppt(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((int)(results[0]));
        }

        /// <remarks/>
        public void WS_PR_DeleteApptAsync(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID)
        {
            this.WS_PR_DeleteApptAsync(s44D_vConnectionID, s44D_vT_Sender, s44D_vL_ID, null);
        }

        /// <remarks/>
        public void WS_PR_DeleteApptAsync(string s44D_vConnectionID, string s44D_vT_Sender, int s44D_vL_ID, object userState)
        {
            if ((this.WS_PR_DeleteApptOperationCompleted == null))
            {
                this.WS_PR_DeleteApptOperationCompleted = new System.Threading.SendOrPostCallback(this.OnWS_PR_DeleteApptOperationCompleted);
            }
            this.InvokeAsync("WS_PR_DeleteAppt", new object[] {
                                 s44D_vConnectionID,
                                 s44D_vT_Sender,
                                 s44D_vL_ID}, this.WS_PR_DeleteApptOperationCompleted, userState);
        }

        private void OnWS_PR_DeleteApptOperationCompleted(object arg)
        {
            if ((this.WS_PR_DeleteApptCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.WS_PR_DeleteApptCompleted(this, new WS_PR_DeleteApptCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void s44D_CloseConnectionCompletedEventHandler(object sender, s44D_CloseConnectionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class s44D_CloseConnectionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal s44D_CloseConnectionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void s44D_NewConnectionCompletedEventHandler(object sender, s44D_NewConnectionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class s44D_NewConnectionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal s44D_NewConnectionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vConnectionID
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_FindApptsCompletedEventHandler(object sender, WS_FindApptsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_FindApptsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_FindApptsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_MakeApptCompletedEventHandler(object sender, WS_MakeApptCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_MakeApptCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_MakeApptCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_GetDrsCompletedEventHandler(object sender, WS_GetDrsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_GetDrsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_GetDrsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_FindPatientsForDayCompletedEventHandler(object sender, WS_FindPatientsForDayCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_FindPatientsForDayCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_FindPatientsForDayCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_SearchAddressBookCompletedEventHandler(object sender, WS_SearchAddressBookCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_SearchAddressBookCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_SearchAddressBookCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PlaceDocumentCompletedEventHandler(object sender, WS_PlaceDocumentCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PlaceDocumentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PlaceDocumentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_GetTemplatesCompletedEventHandler(object sender, WS_GetTemplatesCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_GetTemplatesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_GetTemplatesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_FindByMedicareCardCompletedEventHandler(object sender, WS_FindByMedicareCardCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_FindByMedicareCardCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_FindByMedicareCardCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_MarkArrivedCompletedEventHandler(object sender, WS_MarkArrivedCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_MarkArrivedCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_MarkArrivedCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_SearchPatientsCompletedEventHandler(object sender, WS_SearchPatientsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_SearchPatientsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_SearchPatientsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_SearchAB_ModCompletedEventHandler(object sender, WS_SearchAB_ModCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_SearchAB_ModCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_SearchAB_ModCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_GetVersionCompletedEventHandler(object sender, WS_GetVersionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_GetVersionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_GetVersionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public float Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((float)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_GetFreeApptsCompletedEventHandler(object sender, WS_PR_GetFreeApptsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_GetFreeApptsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_GetFreeApptsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_MakeApptCompletedEventHandler(object sender, WS_PR_MakeApptCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_MakeApptCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_MakeApptCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_PtCreated
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_GetTypesCompletedEventHandler(object sender, WS_PR_GetTypesCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_GetTypesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_GetTypesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_CancelApptCompletedEventHandler(object sender, WS_PR_CancelApptCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_CancelApptCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_CancelApptCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_GetTakenApptsCompletedEventHandler(object sender, WS_PR_GetTakenApptsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_GetTakenApptsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_GetTakenApptsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_PatientAdminCompletedEventHandler(object sender, WS_PR_PatientAdminCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_PatientAdminCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_PatientAdminCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_NextAvailableCompletedEventHandler(object sender, WS_PR_NextAvailableCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_NextAvailableCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_NextAvailableCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_GetDrsCompletedEventHandler(object sender, WS_PR_GetDrsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_GetDrsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_GetDrsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_GetVacantApptsCompletedEventHandler(object sender, WS_PR_GetVacantApptsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_GetVacantApptsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_GetVacantApptsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public byte[] s44D_vX_Output
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_MakeApptByTimeCompletedEventHandler(object sender, WS_PR_MakeApptByTimeCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_MakeApptByTimeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_MakeApptByTimeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }

        /// <remarks/>
        public string s44D_vT_PtCreated
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void WS_PR_DeleteApptCompletedEventHandler(object sender, WS_PR_DeleteApptCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class WS_PR_DeleteApptCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal WS_PR_DeleteApptCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public int Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[0]));
            }
        }
    }


}