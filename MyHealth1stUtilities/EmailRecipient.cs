﻿using System;
namespace MyHealth1stUtilities
{
    public class EmailRecipient
    {
        public string BranchIdentifier  { get; set; }
        public string PMSPatientID { get; set; }
        public string PMSUserIdentifier  { get; set; }
        public string RecipientLastVisitDate { get; set; }
        public string RecipientWaitseen { get; set; }
        public string PractitionerName { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public string StoreSuburb { get; set; }
        public string StoreState { get; set; }
        public string StorePostcode { get; set; }
        public string RecipientFirstName { get; set; }
        public string RecipientLastName { get; set; }
        public string RecipientPostcode { get; set; }
        public string RecipientState { get; set; }
        public string RecipientEmail { get; set; }
        public string RecipientDOB { get; set; }
        public string RecipientGender { get; set; }
        public string RecipientHealthFund { get; set; }

        public EmailRecipient()
        {
        }
        
    }

    
}

