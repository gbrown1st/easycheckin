﻿using Newtonsoft.Json.Linq;
using PubNubMessaging.Core;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Net;
using System.ServiceProcess;
using System;
using System.Xml.Linq;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace MyHealth1stUtilities
{
    public partial class MyHealth1stUtilities : ServiceBase
    {
        static Pubnub pubnub;
        string publishKey;
        string subscribeKey;
        string channelName;
        static private string currentDBURL;

        public MyHealth1stUtilities()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            LogWriter.Logger.Info("Starting service... ");
            initPubnubClient();
        }

        public void initPubnubClient()
        {
            LogWriter.Logger.Info("Starting PubNub... ");
            publishKey = ConfigurationManager.AppSettings["PublishKey"];
            subscribeKey = ConfigurationManager.AppSettings["SubscribeKey"];
            channelName = ConfigurationManager.AppSettings["ChannelName"];
            pubnub = new Pubnub(
            publishKey,
            subscribeKey
            );
            subscribe();
        }

        public void subscribe()
        {
            pubnub.Subscribe<string>(channelName, DisplaySubscribeReturnMessage, DisplaySubscribeConnectStatusMessage, DisplayErrorMessage);
        }

        protected override void OnStop()
        {
            LogWriter.Logger.Info("Stopping service... ");
        }

        void DisplaySubscribeReturnMessage(string result)
        {
            LogWriter.Logger.Info("SUBSCRIBE REGULAR CALLBACK: " + result);
            if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(result.Trim()))
            {
                List<object> deserializedMessage = pubnub.JsonPluggableLibrary.DeserializeToListOfObject(result);
                if (deserializedMessage != null && deserializedMessage.Count > 0)
                {
                    object subscribedObject = (object)deserializedMessage[0];
                    if (subscribedObject != null)
                    {

                        string resultActualMessage = pubnub.JsonPluggableLibrary.SerializeToJsonString(subscribedObject);
                        LogWriter.Logger.Info("Pubnub resultActualMessage: " + resultActualMessage);
                        if (resultActualMessage.Contains("efPremier"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            connectToOptomatePremier();
                        }
                        else if (resultActualMessage.Contains("efOptomate"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            connectToOptomate();
                        }
                        else if (resultActualMessage.Contains("efSunix"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            connectToSunix();
                        }
                        else if (resultActualMessage.Contains("efD4W"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            connectToD4W();
                        }
                        else if (resultActualMessage.Contains("bpsSMSLast5Mins"))
                        {
                            LogWriter.Logger.Info("Pubnub bpsSMSLast5Mins message received: " + resultActualMessage);
                            JObject o = JObject.Parse(resultActualMessage);
                            var message = new PubNubSMSReminderMessage();
                            message.practiceID = (string)o["practiceID"];
                            message.timeInterval = (int)o["timeInterval"];
                            connectToBPSMSLast5Mins(message);
                        }
                        else if (resultActualMessage.Contains("bpsSMSHours"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            JObject o = JObject.Parse(resultActualMessage);
                            var message = new PubNubSMSReminderMessage();
                            message.practiceID = (string)o["practiceID"];
                            message.timeInterval = (int)o["timeInterval"];
                            connectToBPSMSHours(message);
                        }
                        else if (resultActualMessage.Contains("bpsSMSDays"))
                        {
                            LogWriter.Logger.Info("Pubnub bpSMSDays message received: " + resultActualMessage);
                            JObject o = JObject.Parse(resultActualMessage);
                            var message = new PubNubSMSReminderMessage();
                            message.practiceID = (string)o["practiceID"];
                            message.timeInterval = (int)o["timeInterval"];
                            connectToBPSMSDays(message);
                        }
                        else if (resultActualMessage.Contains("my1stPatientCheckin"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            JObject o = JObject.Parse(resultActualMessage);
                            var message = new PubNubMessage();
                            message.i = (string)o["my1stPatientCheckin"];
                            message.p = (string)o["p"];
                            message.f = (string)o["f"];
                            message.l = (string)o["l"];
                            message.d = (string)o["d"];
                            message.g = (string)o["g"];
                            message.m = (string)o["m"];
                            LogWriter.Logger.Info("Pubnub message decoded: " + message.ToString());
                            if (message.p == "bp")
                            {
                                arrivePatientBestPractice(message, message.i);
                            }
                            else if (message.p == "genie")
                            {
                                arrivePatientGenie(message, message.i);
                            }
                            else if (message.p == "pracsoft")
                            {
                                arrivePatientPracsoft(message, message.i);
                            }
                            else if (message.p == "bluechip")
                            {
                                arrivePatientBlueChip(message, message.i);
                            }
                        }
                        else if (resultActualMessage.Contains(value: "getWaitingRoom"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            JObject o = JObject.Parse(resultActualMessage);
                            var message = new PubNubMessage();
                            message.i = (string)o["getWaitingRoom"];
                            message.p = (string)o["p"];
                            message.f = "";
                            message.l = "";
                            message.d = "";
                            message.g = "";
                            message.m = "";
                            LogWriter.Logger.Info("Pubnub message decoded: " + message.ToString());
                            if (message.p == "pracsoft")
                            {
                                GetPracsoftWaitingRoom(message, message.i);
                            }
                        }
                        else if (resultActualMessage.Contains("PubNubHeartBeatBP"))
                        {
                            LogWriter.Logger.Info("Pubnub message received: " + resultActualMessage);
                            JObject o = JObject.Parse(resultActualMessage);
                            var message = new PubNubSMSReminderMessage();
                            message.practiceID = (string)o["practiceID"];
                            message.timeInterval = (int)o["timeInterval"];
                            checkPubNubHealthBP(message);
                        }
                    }
                }
            }
        }

        private void checkPubNubHealthBP(PubNubSMSReminderMessage message)
        {
            string dbServer = ConfigurationManager.AppSettings["bp.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["bp.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["bp.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["bp.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["bp.dbName"];
            // Open connection to the database
            string bpConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("bpConnectionURL = " + bpConnectionURL);

            var connectedToDatabase = "";

            if (IsServerConnected(bpConnectionURL))
            {
                connectedToDatabase = "Connected to the database.";
            }
            else
            {
                connectedToDatabase = "Cannot connect to the database."; ;
            }
            LogWriter.Logger.Info("dbConnection = " + bpConnectionURL);

            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                var url = "https://easyfeedback.myhealth1st.com.au/pubnub_heartbeat.php";
                NameValueCollection collection = new NameValueCollection();
                collection.Add("practiceID", message.practiceID);
                collection.Add("dbConnection", connectedToDatabase);
                byte[] bytes = client.UploadValues(url, "POST", collection);
            }

        }

        private void connectToBPSMSLast5Mins(PubNubSMSReminderMessage message)
        {
            string dbServer = ConfigurationManager.AppSettings["bp.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["bp.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["bp.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["bp.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["bp.dbName"];
            // Open connection to the database
            string bpConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("bpConnectionURL = " + bpConnectionURL);

            if (IsServerConnected(bpConnectionURL))
            {
                var dataSP = new BestPracticeStoredProcedures(bpConnectionURL);
                var smsReminders = dataSP.getSMSAppointmentsLast5Mins(message.timeInterval);

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var url = "https://easyfeedback.myhealth1st.com.au/best_practice_last_5_mins_sms.php";

                    for (int i = 0; i < smsReminders.Count; i += 50)
                    {

                        var JSONData = JsonConvert.SerializeObject(smsReminders.Skip(i).Take(50));
                        LogWriter.Logger.Info("JSONData(" + i + ") = " + JSONData);

                        NameValueCollection collection = new NameValueCollection();
                        collection.Add("practiceID", message.practiceID);
                        collection.Add("JSONData", JSONData);

                        byte[] bytes = client.UploadValues(url, "POST", collection);

                    }
                }
            }
            else
            {
            }
        }

        private void connectToBPSMSHours(PubNubSMSReminderMessage message)
        {
            string dbServer = ConfigurationManager.AppSettings["bp.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["bp.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["bp.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["bp.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["bp.dbName"];
            // Open connection to the database
            string bpConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("bpConnectionURL = " + bpConnectionURL);

            if (IsServerConnected(bpConnectionURL))
            {
                var dataSP = new BestPracticeStoredProcedures(bpConnectionURL);
                var smsReminders = dataSP.getSMSAppointmentsHours(message.timeInterval);

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var url = "https://easyfeedback.myhealth1st.com.au/best_practice_sms.php";

                    for (int i = 0; i < smsReminders.Count; i += 50)
                    {

                        var JSONData = JsonConvert.SerializeObject(smsReminders.Skip(i).Take(50));
                        LogWriter.Logger.Info("JSONData(" + i + ") = " + JSONData);

                        NameValueCollection collection = new NameValueCollection();
                        collection.Add("practiceID", message.practiceID);
                        collection.Add("JSONData", JSONData);

                        byte[] bytes = client.UploadValues(url, "POST", collection);

                    }
                }
            }
            else
            {
            }
        }

        private void connectToBPSMSDays(PubNubSMSReminderMessage message)
        {
            string dbServer = ConfigurationManager.AppSettings["bp.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["bp.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["bp.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["bp.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["bp.dbName"];
            // Open connection to the database
            string bpConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("bpConnectionURL = " + bpConnectionURL);

            if (IsServerConnected(bpConnectionURL))
            {
                var dataSP = new BestPracticeStoredProcedures(bpConnectionURL);
                var smsReminders = dataSP.getSMSAppointmentsDays(message.timeInterval);

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var url = "https://easyfeedback.myhealth1st.com.au/best_practice_sms.php";

                    for (int i = 0; i < smsReminders.Count; i += 50)
                    {

                        var JSONData = JsonConvert.SerializeObject(smsReminders.Skip(i).Take(50));
                        LogWriter.Logger.Info("JSONData(" + i + ") = " + JSONData);

                        NameValueCollection collection = new NameValueCollection();
                        collection.Add("practiceID", message.practiceID);
                        collection.Add("JSONData", JSONData);

                        byte[] bytes = client.UploadValues(url, "POST", collection);

                    }
                }

            }
            else
            {
            }
        }

        void DisplaySubscribeConnectStatusMessage(string result)
        {
            LogWriter.Logger.Info("SUBSCRIBE CONNECT CALLBACK: " + result);
        }

        void DisplayErrorMessage(PubnubClientError pubnubError)
        {
            LogWriter.Logger.Info("Pubnub error: " + pubnubError.StatusCode);
        }

        public void connectToOptomate()
        {
            string dbServer = ConfigurationManager.AppSettings["dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["dbPassword"];
            string dbName = ConfigurationManager.AppSettings["dbName"];
            // Open connection to the database
            currentDBURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            if (IsServerConnected(currentDBURL))
            {
                var dataSP = new Optomate(currentDBURL);
                var emailRecipients = dataSP.getYesterdaysAppts();

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var url = "https://easyfeedback.myhealth1st.com.au/daily_data_touch.php";

                    for (int i = 0; i < emailRecipients.Count; i += 50)
                    {

                        var JSONData = JsonConvert.SerializeObject(emailRecipients.Skip(i).Take(50));
                        LogWriter.Logger.Info("JSONData(" + i + ") = " + JSONData);

                        NameValueCollection collection = new NameValueCollection();
                        collection.Add("pubnubChannel", channelName);
                        collection.Add("JSONData", JSONData);

                        byte[] bytes = client.UploadValues(url, "POST", collection);

                    }

                }

            }
            else
            {
            }
        }

        public void connectToD4W()
        {
            currentDBURL = ConfigurationManager.AppSettings["d4w.connString"];
            LogWriter.Logger.Info("currentDBURL is " + currentDBURL);
            if (IsODBCServerConnected(currentDBURL))
            {

                var dataSP = new D4W(currentDBURL);
                var emailRecipients = dataSP.getYesterdaysAppts();

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var url = "https://qa-easyfeedback.myhealth1st.com.au/daily_data_d4w.php";

                    for (int i = 0; i < emailRecipients.Count; i += 50)
                    {

                        var JSONData = JsonConvert.SerializeObject(emailRecipients.Skip(i).Take(50));
                        LogWriter.Logger.Info("JSONData(" + i + ") = " + JSONData);

                        NameValueCollection collection = new NameValueCollection();
                        collection.Add("pubnubChannel", channelName);
                        collection.Add("JSONData", JSONData);

                        byte[] bytes = client.UploadValues(url, "POST", collection);

                    }

                }

            }

            else
            {
            }
        }

        public void connectToOptomatePremier()
        {
            string dbServer = ConfigurationManager.AppSettings["premier.dbServer"];
            string dbName = ConfigurationManager.AppSettings["premier.dbName"];
            // Open connection to the database
            currentDBURL = "DRIVER={NexusDB V3.09 Driver};Transport=TCP;SERVER=" + dbServer + ";PORT=16000;DataBase=" + dbName + ";";
            LogWriter.Logger.Info("currentDBURL is " + currentDBURL);
            if (IsODBCServerConnected(currentDBURL))
            {

                var dataSP = new OptomatePremier(currentDBURL);
                var emailRecipients = dataSP.getYesterdaysAppts();

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var url = "https://easyfeedback.myhealth1st.com.au/daily_data_premier.php";

                    for (int i = 0; i < emailRecipients.Count; i += 50)
                    {

                        var JSONData = JsonConvert.SerializeObject(emailRecipients.Skip(i).Take(50));
                        LogWriter.Logger.Info("JSONData(" + i + ") = " + JSONData);

                        NameValueCollection collection = new NameValueCollection();
                        collection.Add("pubnubChannel", channelName);
                        collection.Add("JSONData", JSONData);

                        byte[] bytes = client.UploadValues(url, "POST", collection);

                    }

                }

            }

            else
            {
            }
        }

        public void connectToSunix()
        {
            currentDBURL = ConfigurationManager.AppSettings["sunix.connString"];
            LogWriter.Logger.Info("currentDBURL is " + currentDBURL);
            if (IsOleDbServerConnected(currentDBURL))
            {

                var dataSP = new Sunix(currentDBURL);
                var emailRecipients = dataSP.getYesterdaysAppts();

                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    var url = "https://easyfeedback.myhealth1st.com.au/daily_data_premier.php";

                    for (int i = 0; i < emailRecipients.Count; i += 50)
                    {

                        var JSONData = JsonConvert.SerializeObject(emailRecipients.Skip(i).Take(50));
                        LogWriter.Logger.Info("JSONData(" + i + ") = " + JSONData);

                        NameValueCollection collection = new NameValueCollection();
                        collection.Add("pubnubChannel", channelName);
                        collection.Add("JSONData", JSONData);

                        byte[] bytes = client.UploadValues(url, "POST", collection);

                    }

                }

            }

            else
            {
            }
        }

        private static bool IsOleDbServerConnected(string connectionString)
        {

            try
            {
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    LogWriter.Logger.Info("Connected to the oledb database with " + connectionString);
                    connection.Open();
                    return true;
                }

            }

            catch (OleDbException ex)
            {
                LogWriter.Logger.Info("Couldn't connect to the database..." + ex.ToString());
                return false;
            }
        }

        private static bool IsODBCServerConnected(string connectionString)
        {

            try
            {
                using (OdbcConnection connection = new OdbcConnection(connectionString))
                {
                    LogWriter.Logger.Info("Connected to the odbc database with " + connectionString);
                    connection.Open();
                    return true;
                }

            }

            catch (OdbcException ex)
            {
                LogWriter.Logger.Info("Couldn't connect to the database..." + ex.ToString());
                return false;
            }
        }

        private static bool IsServerConnected(string connectionString)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    LogWriter.Logger.Info("Connected to the database with " + connectionString);
                    connection.Open();
                    return true;
                }

            }

            catch (SqlException)
            {
                LogWriter.Logger.Info("Couldn't connect to the database...");
                return false;
            }
        }

        private string OpenGenieConnection(GenieWebService ws)
        {
            int limit = 0;
            Int32.TryParse(ConfigurationManager.AppSettings["genie.OpenConnectionRetries"], out limit);
            int secondsToRetry = 0;
            Int32.TryParse(ConfigurationManager.AppSettings["genie.OpenConnectionRetryIntervalInSeconds"], out secondsToRetry); int errorCode = 0;
            string connectionString;
            string sender = ConfigurationManager.AppSettings["genie.sender"];
            string password = ConfigurationManager.AppSettings["genie.password"];
            int i = 0;
            while (i <= limit)
            {
                errorCode = ws.s44D_NewConnection(sender, password, out connectionString);
                if (errorCode != 0)
                {
                    i++;
                    LogWriter.Logger.Info("Error when trying to open a connection to genie: " + errorCode + ". Trying again");
                    Thread.Sleep(1000 * secondsToRetry);
                }

                else return connectionString;
            }
            LogWriter.Logger.Info("Error when opening a connection to Genie. Error Code: " + errorCode);
            throw new Exception("Error when opening a connection to Genie. Error Code: " + errorCode);

        }

        public void arrivePatientGenie(PubNubMessage message, string appChannel)
        {

            bool errors = false;
            string messageResponse = "";
            string genieWSURL = ConfigurationManager.AppSettings["genie.webServiceURL"];
            GenieWebService ws = new GenieWebService(genieWSURL);
            string genieConnection = OpenGenieConnection(ws);
            LogWriter.Logger.Info("genieConnection = " + genieConnection);
            string outputAsString;
            DateTime today = DateTime.Today;

            string todayString = today.ToString("yyyy-MM-dd");

            int errorCode = ws.WS_FindPatientsForDay(todayString, out outputAsString);

            if (outputAsString == "")
            {
                errors = true;
            }
            else
            {
                XDocument xmlDoc = XDocument.Parse(outputAsString);
                List<GeniePatientAppointment> result = (from appts in xmlDoc.Descendants("appt")
                                                        select new GeniePatientAppointment
                                                        {
                                                            ApptID = Convert.ToInt32(appts.Element("id").Value),
                                                            PatientID = Convert.ToInt32(appts.Element("ptid").Value),
                                                            FirstName = appts.Element("firstname").Value,
                                                            LastName = appts.Element("surname").Value,
                                                            DOB = appts.Element("dob").Value,
                                                            medicareno = appts.Element("medicareno").Value
                                                        }
                                       ).ToList();
                LogWriter.Logger.Info("dob = " + message.d);
                LogWriter.Logger.Info("results = " + result.Count().ToString());
                List<GeniePatientAppointment> matches = new List<GeniePatientAppointment>();

                foreach (var anonymous in result)
                {

                    string f1 = message.f.Trim().ToUpper();
                    string f2 = anonymous.FirstName.Trim().ToUpper();
                    string l1 = message.l.Trim().ToUpper();
                    string l2 = anonymous.LastName.Trim().ToUpper();
                    string d1 = message.d.Replace("-", "");
                    string d2 = anonymous.DOB;
                    string m1 = message.m;
                    string m2 = anonymous.medicareno;

                    if (String.IsNullOrEmpty(m1))
                    {
                        if (string.CompareOrdinal(d1, d2) == 0)
                        {
                            LogWriter.Logger.Info(d1 + " matches " + d2);
                            matches.Add(anonymous);
                            message.f = anonymous.FirstName;
                            message.l = anonymous.LastName;
                        }
                    }
                    else
                    {
                        if (string.CompareOrdinal(m1, m2) == 0)
                        {
                            LogWriter.Logger.Info(m1 + " matches " + m2);
                            matches.Add(anonymous);
                            message.f = anonymous.FirstName;
                            message.l = anonymous.LastName;
                        }
                    }

                }
                LogWriter.Logger.Info("matches.Count() " + matches.Count());
                if (matches.Count() == 1)
                {
                    ws.WS_MarkArrived(matches[0].ApptID);
                    LogWriter.Logger.Info("arriving " + matches[0].ApptID);
                }
                else
                {
                    errors = true;
                }
            }


            if (errors)
            {
                messageResponse = "Sorry you will need to go to the front desk to check in.";
            }
            else
            {
                messageResponse = messageResponse = message.f + " " + message.l + " you are checked in.**Please take a seat.";
            }

            var arriveResponse = new ArrivalResponse();
            arriveResponse.appChannel = appChannel;
            arriveResponse.messageResponse = messageResponse;
            LogWriter.Logger.Info("arriveResponse = " + arriveResponse.ToString());
            pubnub.Publish<string>(appChannel, messageResponse, DisplaySubscribeReturnMessage, DisplayErrorMessage);

        }

        public void GetPracsoftWaitingRoom(PubNubMessage message, string appChannel)
        {

            string dbServer = ConfigurationManager.AppSettings["pracsoft.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["pracsoft.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["pracsoft.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["pracsoft.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["pracsoft.dbName"];
            // Open connection to the database
            string pracsoftConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("pracsoftConnectionURL = " + pracsoftConnectionURL);
            var arriveSP = new PracsoftDBProcedures(pracsoftConnectionURL);
            var waitingRoom = arriveSP.getAverageWaiting();
            pubnub.Publish<string>(appChannel, waitingRoom, DisplaySubscribeReturnMessage, DisplayErrorMessage);

        }

        public void arrivePatientBlueChip (PubNubMessage message, string appChannel)
        {

            bool errors = false;
            string messageResponse = "";
            string dbServer = ConfigurationManager.AppSettings["pracsoft.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["pracsoft.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["pracsoft.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["pracsoft.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["pracsoft.dbName"];
            // Open connection to the database
            string bluechipConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("bluechipConnectionURL = " + bluechipConnectionURL);
            var arriveSP = new BlueChipStoredProcedures(bluechipConnectionURL);
            var apptPatient = new PracsoftPatientAppointment();
            if (message.m == "")
            {
                message.g = message.g.Substring(0, 1);
                message.g = message.g.ToUpper();
                var json = arriveSP.getAppointmentIDByGenderDOB(message.g, message.d);
                LogWriter.Logger.Info(json);
                var patientAppts = JsonConvert.DeserializeObject<PracsoftPatientAppointment[]>(json);
                if (patientAppts.Length != 1)
                {
                    errors = true;
                }
                else
                {
                    apptPatient = patientAppts[0];
                    var address = apptPatient.STREET_LINE_1;
                    address = address.Trim();
                    if (!String.IsNullOrEmpty(address) && address.IndexOf(' ') > -1)
                    {
                        address = arriveSP.HideCharacters(address);
                    }
                    apptPatient.STREET_LINE_1 = address;
                    var mobile = apptPatient.PHONE_MOBILE;
                    mobile = mobile.Trim();
                    if (mobile.Length > 3)
                    {
                        var last3 = mobile.Substring(mobile.Length - 3);
                        apptPatient.PHONE_MOBILE = "*******" + last3;
                    }
                    else
                    {
                        apptPatient.PHONE_MOBILE = mobile;
                    }
                    LogWriter.Logger.Info(apptPatient.ToString());
                    var addToWaitroom = arriveSP.addToWaitroom(apptPatient.ApptID);
                    if (addToWaitroom != 1)
                    {
                       errors = true;
                    }
                }
            }
            else
            {
                LogWriter.Logger.Info("message.m = " + message.m);
                var json = arriveSP.getAppointmentIDByMedicaredNumber(message.m);
                LogWriter.Logger.Info(json);
                var patientAppts = JsonConvert.DeserializeObject<PracsoftPatientAppointment[]>(json);
                if (patientAppts.Length != 1)
                {
                    errors = true;
                }
                else
                {
                    apptPatient = patientAppts[0];
                    var address = apptPatient.STREET_LINE_1;
                    address = address.Trim();
                    if (!String.IsNullOrEmpty(address) && address.IndexOf(' ') > -1)
                    {
                        address = arriveSP.HideCharacters(address);
                    }
                    apptPatient.STREET_LINE_1 = address;
                    var mobile = apptPatient.PHONE_MOBILE;
                    mobile = mobile.Trim();
                    if (mobile.Length > 3)
                    {
                        var last3 = mobile.Substring(mobile.Length - 3);
                        apptPatient.PHONE_MOBILE = "*******" + last3;
                    }
                    else
                    {
                        apptPatient.PHONE_MOBILE = mobile;
                    }
                    LogWriter.Logger.Info(apptPatient.ToString());
                    var addToWaitroom = arriveSP.addToWaitroom(apptPatient.ApptID);
                    if (addToWaitroom != 1)
                    {
                        errors = true;
                    }
                }

            }

            if (message.m == "")
            {
                if (errors)
                {
                    messageResponse = "We're sorry, please see the front desk to check in.";
                }
                else
                {
                    if (string.IsNullOrEmpty(apptPatient.STREET_LINE_1) || string.IsNullOrEmpty(apptPatient.CITY) || string.IsNullOrEmpty(apptPatient.POSTCODE) || string.IsNullOrEmpty(apptPatient.PHONE_MOBILE))
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nYou will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                    else
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|You will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                }
            }
            else
            {
                if (errors)
                {
                    messageResponse = "We're sorry, please see the front desk to check in.";
                }
                else
                {
                    if (string.IsNullOrEmpty(apptPatient.STREET_LINE_1) || string.IsNullOrEmpty(apptPatient.CITY) || string.IsNullOrEmpty(apptPatient.POSTCODE) || string.IsNullOrEmpty(apptPatient.PHONE_MOBILE))
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nYou will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                    else
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|You will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                }
            }

            var arriveResponse = new ArrivalResponse();
            arriveResponse.appChannel = appChannel;
            arriveResponse.messageResponse = messageResponse;
            LogWriter.Logger.Info("arriveResponse = " + arriveResponse.ToString());
            //MessageBox.Show("arriveResponse = " + arriveResponse.ToString());
            pubnub.Publish<string>(appChannel, messageResponse, DisplaySubscribeReturnMessage, DisplayErrorMessage);

        }

        public void arrivePatientPracsoft(PubNubMessage message, string appChannel)
        {

            bool errors = false;
            string messageResponse = "";
            string dbServer = ConfigurationManager.AppSettings["pracsoft.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["pracsoft.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["pracsoft.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["pracsoft.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["pracsoft.dbName"];
            // Open connection to the database
            string pracsoftConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("pracsoftConnectionURL = " + pracsoftConnectionURL);
            var arriveSP = new PracsoftDBProcedures(pracsoftConnectionURL);
            var apptPatient = new PracsoftPatientAppointment();
            if (message.m == "")
            {
                message.g = message.g.Substring(0, 1);
                message.g = message.g.ToUpper();
                var json = arriveSP.getAppointmentIDByGenderDOB(message.g, message.d);
                LogWriter.Logger.Info(json);
                var patientAppts = JsonConvert.DeserializeObject<PracsoftPatientAppointment[]>(json);
                if (patientAppts.Length != 1)
                {
                    errors = true;
                }
                else
                {
                    apptPatient = patientAppts[0];
                    var address = apptPatient.STREET_LINE_1;
                    address = address.Trim();
                    if (!String.IsNullOrEmpty(address) && address.IndexOf(' ') > -1)
                    {
                        address = arriveSP.HideCharacters(address);
                    }
                    apptPatient.STREET_LINE_1 = address;
                    var mobile = apptPatient.PHONE_MOBILE;
                    mobile = mobile.Trim();
                    if (mobile.Length > 3)
                    {
                        var last3 = mobile.Substring(mobile.Length - 3);
                        apptPatient.PHONE_MOBILE = "*******" + last3;
                    }
                    else
                    {
                        apptPatient.PHONE_MOBILE = mobile;
                    }
                    LogWriter.Logger.Info(apptPatient.ToString());
                    var displayOrder = arriveSP.getIndentCurrent();
                    var waitRoomID = arriveSP.getWaitroomID(apptPatient.ApptID.ToString());
                    if (waitRoomID == 0)
                    {
                        var addToWaitroom = arriveSP.addToWaitroom(apptPatient.PatientID, displayOrder, apptPatient.ApptID, apptPatient.Practitioner, apptPatient.When);
                        if (addToWaitroom == 1)
                        {
                            var updateAppointment = arriveSP.updateAppointment(apptPatient.ApptID);
                            if (updateAppointment != 1)
                            {
                                errors = true;
                            }
                        }
                        else
                        {
                            errors = true;
                        }
                    }
                }
            }
            else
            {
                LogWriter.Logger.Info("message.m = " + message.m);
                var json = arriveSP.getAppointmentIDByMedicaredNumber(message.m);
                LogWriter.Logger.Info(json);
                var patientAppts = JsonConvert.DeserializeObject<PracsoftPatientAppointment[]>(json);
                if (patientAppts.Length != 1)
                {
                    errors = true;
                }
                else
                {
                    apptPatient = patientAppts[0];
                    var address = apptPatient.STREET_LINE_1;
                    address = address.Trim();
                    if (!String.IsNullOrEmpty(address) && address.IndexOf(' ') > -1)
                    {
                        address = arriveSP.HideCharacters(address);
                    }
                    apptPatient.STREET_LINE_1 = address;
                    var mobile = apptPatient.PHONE_MOBILE;
                    mobile = mobile.Trim();
                    if (mobile.Length > 3)
                    {
                        var last3 = mobile.Substring(mobile.Length - 3);
                        apptPatient.PHONE_MOBILE = "*******" + last3;
                    }
                    else
                    {
                        apptPatient.PHONE_MOBILE = mobile;
                    }
                    LogWriter.Logger.Info(apptPatient.ToString());
                    var displayOrder = arriveSP.getIndentCurrent();
                    var waitRoomID = arriveSP.getWaitroomID(apptPatient.ApptID.ToString());
                    if (waitRoomID == 0)
                    {
                        var addToWaitroom = arriveSP.addToWaitroom(apptPatient.PatientID, displayOrder, apptPatient.ApptID, apptPatient.Practitioner, apptPatient.When);
                        if (addToWaitroom == 1)
                        {
                            var updateAppointment = arriveSP.updateAppointment(apptPatient.ApptID);
                            if (updateAppointment != 1)
                            {
                                errors = true;
                            }
                        }
                        else
                        {
                            errors = true;
                        }
                    }
                }

            }

            if (message.m == "")
            {
                if (errors)
                {
                    messageResponse = "We're sorry, please see the front desk to check in.";
                }
                else
                {
                    if (string.IsNullOrEmpty(apptPatient.STREET_LINE_1) || string.IsNullOrEmpty(apptPatient.CITY) || string.IsNullOrEmpty(apptPatient.POSTCODE) || string.IsNullOrEmpty(apptPatient.PHONE_MOBILE))
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nYou will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                    else
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|You will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                }
            }
            else
            {
                if (errors)
                {
                    messageResponse = "We're sorry, please see the front desk to check in.";
                }
                else
                {
                    if (string.IsNullOrEmpty(apptPatient.STREET_LINE_1) || string.IsNullOrEmpty(apptPatient.CITY) || string.IsNullOrEmpty(apptPatient.POSTCODE) || string.IsNullOrEmpty(apptPatient.PHONE_MOBILE))
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nYou will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                    else
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|You will be the next patient to see " + apptPatient.name + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There is 1 patient waiting to see " + apptPatient.name + " before you.";
                        }
                        else
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FIRST_NAME + "|" + apptPatient.STREET_LINE_1 + "|" + apptPatient.CITY + " " + apptPatient.POSTCODE + "|" + apptPatient.PHONE_MOBILE + "|There are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.name + " before you.";
                        }
                    }
                }
            }

            var arriveResponse = new ArrivalResponse();
            arriveResponse.appChannel = appChannel;
            arriveResponse.messageResponse = messageResponse;
            LogWriter.Logger.Info("arriveResponse = " + arriveResponse.ToString());
            //MessageBox.Show("arriveResponse = " + arriveResponse.ToString());
            pubnub.Publish<string>(appChannel, messageResponse, DisplaySubscribeReturnMessage, DisplayErrorMessage);

        }


        public void arrivePatientBestPractice(PubNubMessage message, string appChannel)
        {
            bool errors = false;
            string messageResponse = "";
            string dbServer = ConfigurationManager.AppSettings["bp.dbServer"];
            string dbInstance = ConfigurationManager.AppSettings["bp.dbInstance"];
            string dbUsername = ConfigurationManager.AppSettings["bp.dbUsername"];
            string dbPassword = ConfigurationManager.AppSettings["bp.dbPassword"];
            string dbName = ConfigurationManager.AppSettings["bp.dbName"];
            // Open connection to the database
            string bpConnectionURL = string.Format("Data Source={0}\\{1};Initial Catalog={2};User ID={3};Password={4}", dbServer, dbInstance, dbName, dbUsername, dbPassword);
            LogWriter.Logger.Info("bpConnectionURL = " + bpConnectionURL);
            var arriveSP = new BestPracticeStoredProcedures(bpConnectionURL);
            var apptPatient = new BestPracticePatientAppointment();
            if (message.m == "")
            {
                var patientAppts = arriveSP.getAppointmentIDByGenderDOB(message.g, message.d);
                if (patientAppts.Count != 1)
                {
                    errors = true;
                }
                else
                {
                    apptPatient = patientAppts[0];
                    var addToWaitroom = arriveSP.addToWaitroom(apptPatient.ApptID);
                    LogWriter.Logger.Info("addToWaitroom = " + addToWaitroom.ToString());
                }
            }
            else
            {
                LogWriter.Logger.Info("message.m = " + message.m);
                var patientAppts = arriveSP.getAppointmentIDByMedicaredNumber(message.m);

                if (patientAppts.Count != 1)
                {
                    errors = true;
                }
                else
                {
                    apptPatient = patientAppts[0];
                    LogWriter.Logger.Info("apptPatient = " + apptPatient.ToString());
                    var addToWaitroom = arriveSP.addToWaitroom(apptPatient.ApptID);
                    LogWriter.Logger.Info("addToWaitroom = " + addToWaitroom.ToString());
                }
            }

            LogWriter.Logger.Info("errors = " + errors.ToString());  

            if (message.m == "")
            {
                if (errors)
                {
                    messageResponse = "We're sorry, please see the front desk to check in.";
                }
                else
                {
                    if (string.IsNullOrEmpty(apptPatient.address) || string.IsNullOrEmpty(apptPatient.city) || string.IsNullOrEmpty(apptPatient.postcode) || string.IsNullOrEmpty(apptPatient.mobile))
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nYou will be the next patient to see " + apptPatient.practitioner + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere is 1 patient waiting to see " + apptPatient.practitioner + " before you.";
                        }
                        else
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.practitioner + " before you.";
                        }
                    }
                    else
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "Welcome " + apptPatient.FirstName + "|" + apptPatient.address + "|" + apptPatient.city + " " + apptPatient.postcode + "|" + apptPatient.mobile + "|You will be the next patient to see " + apptPatient.practitioner + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FirstName + "|" + apptPatient.address + "|" + apptPatient.city + " " + apptPatient.postcode + "|" + apptPatient.mobile + "|There is 1 patient waiting to see " + apptPatient.practitioner + " before you.";
                        }
                        else
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FirstName + "|" + apptPatient.address + "|" + apptPatient.city + " " + apptPatient.postcode + "|" + apptPatient.mobile + "|There are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.practitioner + " before you.";
                        }
                    }
                }
            }
            else
            {
                if (errors)
                {
                    messageResponse = "We're sorry, please see the front desk to check in.";
                }
                else
                {
                    if (string.IsNullOrEmpty(apptPatient.address) || string.IsNullOrEmpty(apptPatient.city) || string.IsNullOrEmpty(apptPatient.postcode) || string.IsNullOrEmpty(apptPatient.mobile))
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nYou will be the next patient to see " + apptPatient.practitioner + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere is 1 patient waiting to see " + apptPatient.practitioner + " before you.";
                        }
                        else
                        {
                            messageResponse = "We are missing contact details for you. Please see the front desk.\n\nThere are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.practitioner + " before you.";
                        }
                    }
                    else
                    {
                        if (apptPatient.numberWaiting == 0)
                        {
                            messageResponse = "Welcome " + apptPatient.FirstName + "|" + apptPatient.address + "|" + apptPatient.city + " " + apptPatient.postcode + "|" + apptPatient.mobile + "|You will be the next patient to see " + apptPatient.practitioner + ".";
                        }
                        else if (apptPatient.numberWaiting == 1)
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FirstName + "|" + apptPatient.address + "|" + apptPatient.city + " " + apptPatient.postcode + "|" + apptPatient.mobile + "|There is 1 patient waiting to see " + apptPatient.practitioner + " before you.";
                        }
                        else
                        {
                            messageResponse = messageResponse = "Welcome " + apptPatient.FirstName + "|" + apptPatient.address + "|" + apptPatient.city + " " + apptPatient.postcode + "|" + apptPatient.mobile + "|There are " + apptPatient.numberWaiting + " patients waiting to see " + apptPatient.practitioner + " before you.";
                        }
                    }
                }
            }

            var arriveResponse = new ArrivalResponse();
            arriveResponse.appChannel = appChannel;
            arriveResponse.messageResponse = messageResponse;
            LogWriter.Logger.Info("arriveResponse = " + arriveResponse.ToString());
            //MessageBox.Show("arriveResponse = " + arriveResponse.ToString());
            pubnub.Publish<string>(appChannel, messageResponse, DisplaySubscribeReturnMessage, DisplayErrorMessage);

        }

    }
}
