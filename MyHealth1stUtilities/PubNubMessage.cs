﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHealth1stUtilities
{
    /// <summary>
	/// Description of PubNubMessage.
	/// </summary>
	public class PubNubMessage
    {

        public string i { get; set; }
        public string p { get; set; }
        public string f { get; set; }
        public string l { get; set; }
        public string d { get; set; }
        public string g { get; set; }
        public string m { get; set; }

        public PubNubMessage()
        {
        }

        override public string ToString()
        {
            return string.Format("id: {0} - pms: {1} - firstName: {2} - lastName: {3} - dob: {4} - gender: {5} - medicareno: {6}", i, p, f, l, d, g, m);
        }

    }
}
