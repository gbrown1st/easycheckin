﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyHealth1stUtilities
{
    class GeniePatientAppointment
    {
        public int ApptID { get; set; }
        public int PatientID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string medicareno { get; set; }

        public GeniePatientAppointment()
        {
        }

        override public string ToString()
        {
            return string.Format("ApptID: {0} - PatientID: {1} - FirstName: {2} - LastName: {3} - DOB: {4} - Medicare No: {5}", ApptID, PatientID, FirstName, LastName, DOB, medicareno);
        }
    }
}
