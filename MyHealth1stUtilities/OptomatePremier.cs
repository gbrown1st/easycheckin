﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;

namespace MyHealth1stUtilities
{
    class OptomatePremier
    {
        string connectionString;

        const string GET_YESTERDAYS_APPTS = @"#T 0
SET PASSWORDS ADD '6!6@6#5$3%9';
SELECT
   APPNEXUS.BRANCH AS BranchIdentifier,
   TRIM(cast(APPNEXUS.CLIENTNUM as varchar(50))) AS PMSPatientID,
   APPNEXUS.RESOURCEID AS PMSUserIdentifier,
   substring(cast(APPNEXUS.""START"" as varchar(50)) from 11 for 19) AS RecipientLastVisitDate,
   TRIM(cast(APPNEXUS.WAITSEEN as varchar(50))) AS Waitseen,
   Optom.NAME AS PractitionerName,
   BRANCH.NAME AS StoreName,
   BRANCH.ADDRESS1 AS StoreAddress,
   BRANCH.SUBURB AS StoreSuburb,
   BRANCH.STATE AS StoreState,
   BRANCH.POSTCODE AS StorePostcode,
   CLIENT.GIVEN AS RecipientFirstName,
   CLIENT.SURNAME AS RecipientLastName,
   CLIENT.POSTCODE AS RecipientPostcode,
   CLIENT.STATE AS RecipientState,
   CLIENT.EMAIL AS RecipientEmail,
   substring(cast(CLIENT.BIRTHDATE as varchar(50)) from 6 for 10) AS RecipientDOB,
   CLIENT.SEX AS RecipientGender,
   CLIENT.HEALTHFUND AS RecipientHealthFund
 FROM
   APPNEXUS
   INNER JOIN BRANCH ON APPNEXUS.BRANCH = BRANCH.CODE
   INNER JOIN Optom ON APPNEXUS.RESOURCEID = Optom.CODE
   INNER JOIN CLIENT ON APPNEXUS.CLIENTNUM = CLIENT.NUMBER
WHERE CAST (""START"" AS DATE) = (CURRENT_DATE- INTERVAL '1' DAY)";


        public OptomatePremier(string connectionString)
        {
            this.connectionString = connectionString;
        }

        private OdbcConnection GetConnection()
        {
            var connection = new OdbcConnection(connectionString);
            return connection;
        }

        public List<EmailRecipient> getYesterdaysAppts()
        {
            var connection = new OdbcConnection(connectionString);
            LogWriter.Logger.Info("getYesterdaysAppts = " + connectionString);
            var emailRecipientList = new List<EmailRecipient>();
            try
            {
                using (connection)
                {
                    connection.Open();
                    OdbcCommand command = new OdbcCommand(GET_YESTERDAYS_APPTS, connection);
                    OdbcDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        var er = new EmailRecipient();
                        er.BranchIdentifier = MyToString(reader.GetValue(0));
                        er.PMSPatientID = MyToString(reader.GetValue(1));
                        er.PMSUserIdentifier = MyToString(reader.GetValue(2));
                        er.RecipientLastVisitDate = MyToString(reader.GetValue(3));
                        er.RecipientWaitseen = MyToString(reader.GetValue(4));
                        er.PractitionerName = MyToString(reader.GetValue(5));
                        er.StoreName = MyToString(reader.GetValue(6));
                        er.StoreAddress = MyToString(reader.GetValue(7));
                        er.StoreSuburb = MyToString(reader.GetValue(8));
                        er.StoreState = MyToString(reader.GetValue(9));
                        er.StorePostcode = MyToString(reader.GetValue(10));
                        er.RecipientFirstName = MyToString(reader.GetValue(11));
                        er.RecipientLastName = MyToString(reader.GetValue(12));
                        er.RecipientPostcode = MyToString(reader.GetValue(13));
                        er.RecipientState = MyToString(reader.GetValue(14));
                        er.RecipientEmail = MyToString(reader.GetValue(15));
                        er.RecipientDOB = MyToString(reader.GetValue(16));
                        er.RecipientGender = MyToString(reader.GetValue(17));
                        er.RecipientHealthFund = MyToString(reader.GetValue(18));
                        emailRecipientList.Add(er);
                    }

                    // Always call Close when done reading.
                    reader.Close();
                }
                   
                    connection.Close();

            }

            catch (Exception e)
            {
                LogWriter.Logger.Info(e.ToString());
            }
            
            return emailRecipientList;
        }

        private static string MyToString(object o)
        {
            if (o == DBNull.Value || o == null)
                return "";

            return o.ToString();
        }
    }
}
