﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyHealth1stUtilities
{
    class PubNubSMSReminderMessage
    {
        public string practiceID { get; set; }
        public int timeInterval { get; set; }

        public PubNubSMSReminderMessage()
        {
        }

        override public string ToString()
        {
            return string.Format("practiceID: {0} - timeInterval: {1}", practiceID, timeInterval);
        }
    }
}
